var $ = jQuery;

var app = {
    init: function () {
        this.statusConnect = null;
        this.connectWebSocket();
        this.router();
        this.accessToken = getCookie('access_token');
        this.scrollTaskList();
        this.menuToggleStyle();
    },
    connectWebSocket: function () {
        this.ws = new ReconnectingWebSocket(
            'wss://pr-store.9ek.ru:49161'
        );

        this.ws.onopen = function (evt) {
            if (this.statusConnect === false) {
                $.notify({
                    message: 'Подключение к серверу восстановлено.'
                },{
                    type: 'success'
                });
                this.statusConnect = true;
            }
            console.log(app.ws);
            console.log("Connected to WebSocket server.");
        };
        this.ws.onclose = function (evt) {
            this.statusConnect = false;
            $.notify({
                message: 'Отключение от сервера.'
            },{
                type: 'danger'
            });
            console.log("Disconnected");
        };
        this.ws.onerror = function (evt, e) {
            this.statusConnect = false;
            $.notify({
                message: 'Проблема с подключением к серверу.'
            },{
                type: 'danger'
            });
            console.log('Error occured: ' + evt.data);
        };
    },
    menuToggleStyle: function () {
        var sidebar = $('#sidebar');
        sidebar.find('.nav-link').on('click', function () {
            sidebar.find('.nav-item').removeClass('active');
            sidebar.find('.nav-link').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.nav-item').addClass('active');
            $(this).closest('.nav').closest('.nav-item').addClass('active');
            $(this).closest('.nav').closest('.nav-item').find('[aria-expanded].nav-link').addClass('active');
        });
    },
    router: function () {
        this.ws.onmessage = function (evt) {
            var data = JSON.parse(evt.data);
            switch (data['method']) {
                case 'handler.error':
                    app.onError(data);
                    break;
                case 'login':
                    app.onLogin(data);
                    break;
                case 'registration':
                    app.onRegistration(data);
                    break;
                case 'task':
                    app.onSelectTaskType(data);
                    app.onLoadTaskType(data);
                // case 'taskTotal':
                // case 'checkCompleteTask':
                // case 'checkExternalAuthorization':
            }
        }
    },
    onError: function (data) {
        if (data['parent_method'] === 'login') {
            $('.form-login [type=submit]').removeClass('d-none');
            $('.form-login .progress').addClass('d-none');
        }
        if (data['parent_method'] === 'registration') {
            $('.form-registration [type=submit]').removeClass('d-none');
            $('.form-registration .progress').addClass('d-none');
        }

        $.notify({
            message: 'Error ' + data['data']['code'] + ': ' + data['data']['description']
        },{
            type: 'danger'
        });
        console.log(data);
    },
    login: function (event) {
        event.preventDefault();
        var form = event.target;
        $(form).find('[type=submit]').addClass('d-none');
        $(form).find('.progress').removeClass('d-none');

        this.ws.send(JSON.stringify({
            'method': 'login',
            'data': {
                email: form.email.value,
                password: form.password.value,
            }
        }));
    },
    onLogin: function (data) {
        if (data['status'] === 'success') {
            setCookie('access_token', data['data']['token']);
            localStorage.setItem('access_token', data['data']['token']);
            window.location.href = '/main';
        }
        console.log(data);
    },
    registration: function (event) {
        event.preventDefault();
        var form = event.target;
        $(form).find('[type=submit]').addClass('d-none');
        $(form).find('.progress').removeClass('d-none');

        this.ws.send(JSON.stringify({
            'method': 'registration',
            'data': {
                email: form.email.value,
                password: form.password.value,
            }
        }));
    },
    onRegistration: function (data) {
        if (data['status'] === 'success') {
            setCookie('access_token', data['data']['token']);
            localStorage.setItem('access_token', data['data']['token']);
            window.location.href = '/main';
        }
        console.log(data);
    },
    selectTaskType: function (event) {
        event.preventDefault();
        var element = event.target;

        $('.task-list').html(render.loader());

        this.ws.send(JSON.stringify({
            'access_token': this.accessToken,
            'context': 'select_task_type',
            'method': 'task',
            'accept': 'text/html',
            'data': {
                social: element.dataset['socialId'],
                type: element.dataset['typeId'],
            }
        }));
    },
    onSelectTaskType: function (data) {
        if (data['status'] === 'success' && data['meta']['context'] === 'select_task_type') {
            $('.task-list').html(data['data']);
        }
        console.log(data);
    },
    scrollTaskList: function () {
        $(window).scroll(function() {
            if ($(window).scrollTop() === $(document).height() - $(window).height()) {
                if ($('.task-list').length > 0) {
                    app.loadTaskType();
                }
            }
        });
    },
    loadTaskType: function () {
        var taskList = $('.task-list .task'),
            taskCount = taskList.length,
            taskType = taskList[0].dataset['typeId'],
            socialType = taskList[0].dataset['socialId']
        ;

        if (this.isBusyLoadTaskType === true) {
            return console.warn('process load task list is busy...');
        }

        this.isBusyLoadTaskType = true;

        this.ws.send(JSON.stringify({
            'access_token': this.accessToken,
            'context': 'load_task_type',
            'method': 'task',
            'accept': 'text/html',
            'data': {
                social: socialType,
                type: taskType,
                offset: taskCount,
            }
        }));
    },
    onLoadTaskType: function (data) {
        if (data['status'] === 'success' && data['meta']['context'] === 'load_task_type') {
            $('.task-list').append(data['data']);
            app.isBusyLoadTaskType = false;
        }
    }
};

var render = {
    loader: function () {
        return $('<div>', {
            'class': 'progress',
            'html': $('<div>', {
                'class': 'progress-bar progress-bar-striped progress-bar-animated',
                'role': 'progressbar',
                'aria-valuenow': 100,
                'aria-valuemin': 0,
                'aria-valuemax': 100,
                'style': 'width: 100%',
            })
        })
    }
};

app.init();