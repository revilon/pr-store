import React from 'react'
import ReactDOM from 'react-dom'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import reducers from './AppBundle/Resources/views/reducers'
import {Router} from 'react-router-dom'
import theme from './AppBundle/Resources/views/themes/default'
import {history} from './AppBundle/Resources/views/utils/history'
import {injectGlobal, ThemeProvider} from 'styled-components'
import ReconnectingWebSocket from 'ReconnectingWebSocket'
import Routes from './AppBundle/Resources/views/Routes'

let webSocket = new ReconnectingWebSocket('wss://pr-store.9ek.ru:49161')

webSocket.onopen = () => {
  console.log('webSocket', webSocket);
}

webSocket.onmessage = (evt) => {
  let parse = JSON.parse(evt.data);
  switch (parse['method']) {
    case 'login':
    case 'registration':
    case 'task':
    case 'taskTotal':
    case 'handler.error':
      console.log(parse);
      break;
    default:
      return
  }
}

injectGlobal`
  body {
    margin: 0;
  }
`

const store = createStore(reducers, applyMiddleware(thunk))

ReactDOM.render(
  <Router history={history}>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Routes webSocket={webSocket}/>
      </ThemeProvider>
    </Provider>
  </Router>
  , document.getElementById('root')
)
