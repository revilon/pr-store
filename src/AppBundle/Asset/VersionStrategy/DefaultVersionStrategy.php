<?php

namespace AppBundle\Asset\VersionStrategy;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

/**
 * Class DefaultVersionStrategy
 * @package AppBundle\Asset\VersionStrategy
 */
class DefaultVersionStrategy implements VersionStrategyInterface
{
    /** @var string */
    private $webPath;

    /** @var string */
    private $format;

    /** @var string */
    private $manifestPath;

    /** @var array */
    private $hashes;

    /**
     * @param string $webPath
     * @param string|null $format
     * @param string $manifestPath
     */
    public function __construct($webPath, $format = null, $manifestPath = null)
    {
        $this->webPath = $webPath;
        $this->format = $format ?? '%s?%s';
        $this->manifestPath = $manifestPath ?? '%s?%s';
    }

    public function getVersion($path): string
    {
        $this->hashes = $this->loadManifest();

        $file = $this->hashes[$path] ?? $path;
        $assetPath = sprintf('%s/%s', $this->webPath, $file);

        if (file_exists($assetPath)) {
            return md5(filemtime($assetPath));
        }

        return '';
    }

    public function applyVersion($path): string
    {
        $version = $this->getVersion($path);

        $file = $this->hashes[$path] ?? $path;

        if ('' === $version) {
            return $file;
        }

        return sprintf($this->format, $file, $version);
    }

    private function loadManifest()
    {
        return json_decode(file_get_contents($this->manifestPath), true);
    }
}
