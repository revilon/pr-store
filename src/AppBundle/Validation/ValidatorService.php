<?php

namespace AppBundle\Validation;

use AppBundle\Entity\EntityInterface;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ValidationService
 * @package AppBundle\Validation
 */
class ValidatorService
{
    /** @var ValidatorInterface */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param EntityInterface $user
     * @throws ApiErrorException
     */
    public function valid(EntityInterface $user): void
    {
        /** @var ConstraintViolation[] $errors */
        $errors = $this->validator->validate($user);

        $messages = [];
        foreach ($errors as $error) {
            $messages[] = sprintf("'%s' - %s", $error->getPropertyPath(), $error->getMessage());
        }
        $message = implode(' ', $messages);

        if ($messages) {
            throw new ApiErrorException(ApiErrorCodeEnum::ERROR_VALIDATE, $message);
        }
    }
}
