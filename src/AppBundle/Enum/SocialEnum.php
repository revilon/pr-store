<?php

namespace AppBundle\Enum;

/**
 * Class SocialEnum
 * @package AppBundle\Enum
 */
final class SocialEnum extends EnumBase
{
    public const INSTAGRAM = 'INSTAGRAM';
    public const VK = 'VK';
    public const FACEBOOK = 'FACEBOOK';
    public const YOUTUBE = 'YOUTUBE';
    public const TWITTER = 'TWITTER';
    public const ODNOKLASSNIKI = 'ODNOKLASSNIKI';

    protected static $choices = [
        self::INSTAGRAM => 'Instagram',
        self::VK => 'Vkontakte',
        self::FACEBOOK => 'Facebook',
        self::YOUTUBE => 'Youtube',
        self::TWITTER => 'Twitter',
        self::ODNOKLASSNIKI => 'Odnoklassniki',
    ];
}
