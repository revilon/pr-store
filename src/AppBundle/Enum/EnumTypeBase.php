<?php

namespace AppBundle\Enum;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class EnumTypeBase
 * @package AppBundle\Enum
 */
abstract class EnumTypeBase extends Type
{
    public const NAME = null;
    public const BASE_ENUM_CLASS = null;

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        $values = implode(
            ', ',
            array_map(
                function ($value) {
                    return sprintf("'%s'", $value);
                },
                static::getValues()
            )
        );

        return sprintf('ENUM(%s)', $values);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!\in_array($value, static::getValues(), true)) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid %s value.',
                $value
            ));
        }
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): ?string
    {
        return static::NAME;
    }

    /**
     * @return array
     */
    public static function getChoices(): array
    {
        /** @var EnumBase $baseEnumClass */
        $baseEnumClass = static::BASE_ENUM_CLASS;

        return $baseEnumClass::getListCombine();
    }

    /**
     * @return array
     */
    public static function getValues(): array
    {
        return array_keys(static::getChoices());
    }
}
