<?php

namespace AppBundle\Enum;

/**
 * Class TaskTypeEnum
 * @package AppBundle\Enum
 */
final class TaskTypeEnum extends EnumBase
{
    public const LIKE = 'LIKE';
    public const SHARE = 'SHARE';
    public const SUBSCRIBE = 'SUBSCRIBE';
    public const COMMENT = 'COMMENT';
    public const VOTE = 'VOTE';
    public const VIEW = 'VIEW';

    protected static $choices = [
        self::LIKE => 'Like',
        self::SHARE => 'Share',
        self::SUBSCRIBE => 'Subscribe',
        self::COMMENT => 'Comment',
        self::VOTE => 'Vote',
        self::VIEW => 'View',
    ];
}
