<?php

namespace AppBundle\Enum;

/**
 * Class AcceptEnum
 * @package AppBundle\Enum
 */
class AcceptEnum extends EnumBase
{
    public const HTML = 'text/html';
    public const JSON = 'application/json';
}
