<?php

namespace AppBundle\Enum;

/**
 * Class ExternalAuthStatusEnum
 * @package AppBundle\Enum
 */
final class ExternalAuthStatusEnum extends EnumBase
{
    public const ACTIVE = 'ACTIVE';
    public const ERROR = 'ERROR';
    public const DISABLED = 'DISABLED';

    protected static $choices = [
        self::ACTIVE => 'Active',
        self::ERROR => 'Error',
        self::DISABLED => 'Disabled',
    ];
}
