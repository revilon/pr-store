<?php

namespace AppBundle\Enum;

use ReflectionClass;

/**
 * Class EnumBase
 * @package AppBundle\Enum
 */
abstract class EnumBase
{
    /** @var array */
    protected static $choices = [];

    /**
     * @return array
     */
    public static function getChoices(): array
    {
        return static::$choices;
    }

    /**
     * @return array
     */
    public static function getValues(): array
    {
        $class = new ReflectionClass(static::class);

        return array_values($class->getConstants());
    }

    /**
     * @return array
     */
    public static function getList(): array
    {
        $class = new ReflectionClass(static::class);

        return $class->getConstants();
    }

    /**
     * @return array
     */
    public static function getListCombine(): array
    {
        $class = new ReflectionClass(static::class);

        return array_combine($class->getConstants(), $class->getConstants());
    }

    /**
     * @param string $value
     * @return int
     */
    public static function getBit($value): int
    {
        $constants = static::getList();

        $index = 0;

        foreach ($constants as $constant) {
            if (mb_strtolower($constant) === mb_strtolower($value)) {
                return 1 << $index;
            }
            $index++;
        }

        return 0;
    }
}
