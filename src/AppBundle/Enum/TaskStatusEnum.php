<?php

namespace AppBundle\Enum;

/**
 * Class TaskStatusEnum
 * @package AppBundle\Enum
 */
final class TaskStatusEnum extends EnumBase
{
    public const NEW = 'NEW';
    public const ACTIVE = 'ACTIVE';
    public const PAUSE = 'PAUSE';
    public const COMPLETE = 'COMPLETE';
    public const ABORT = 'ABORT';

    protected static $choices = [
        self::NEW => 'New',
        self::ACTIVE => 'Active',
        self::PAUSE => 'Pause',
        self::COMPLETE => 'Complete',
        self::ABORT => 'Abort',
    ];
}
