<?php

namespace AppBundle\HttpFoundation;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Swoole\Http\Request as SocketRequest;

/**
 * Class Request
 * @package AppBundle\HttpFoundation
 */
class Request extends SymfonyRequest
{
    /** @var SocketRequest */
    private $socketRequest;

    /** @var int */
    private $fd;

    public function __construct(SocketRequest $socketRequest)
    {
        $this->socketRequest = $socketRequest;
        $this->fd = $socketRequest->fd;

        $query = $socketRequest->get ?? [];
        $request = $socketRequest->post ?? [];
        $cookie = $socketRequest->cookie ?? [];
        $files = $socketRequest->files ?? [];
        $server = $this->preFormatServerRaw() ?? [];
        $content = $this->socketRequest;

        parent::__construct($query, $request, [], $cookie, $files, $server, $content);
    }

    protected function preFormatServerRaw()
    {
        $header = $this->socketRequest->header ?? [];

        $headerList = [];
        foreach ($header as $key => $item) {
            $key = strtoupper('http_' . $key);
            $key = str_replace('-', '_', $key);
            $headerList[$key] = $item;
        }

        $server = array_change_key_case($this->socketRequest->server, CASE_UPPER);


        return array_merge($server, $headerList);
    }
    /**
     * @param int $fd
     * @return Request
     */
    public function setFd(int $fd): Request
    {
        $this->fd = $fd;
        return $this;
    }

    /**
     * @return int
     */
    public function getFd(): int
    {
        return $this->fd;
    }

    /**
     * @param SocketRequest $socketRequest
     * @return Request
     */
    public function setSocketRequest(SocketRequest $socketRequest): Request
    {
        $this->socketRequest = $socketRequest;
        return $this;
    }

    /**
     * @return SocketRequest
     */
    public function getSocketRequest(): SocketRequest
    {
        return $this->socketRequest;
    }
}
