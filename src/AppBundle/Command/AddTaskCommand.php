<?php

namespace AppBundle\Command;

use AppBundle\Entity\Task;
use AppBundle\Enum\SocialEnum;
use AppBundle\Enum\TaskStatusEnum;
use AppBundle\Enum\TaskTypeEnum;
use AppBundle\Service\Task\CheckExternalAuthorizationFormatter;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AddTaskCommand
 * @package AppBundle\Command
 *
 * @property EntityManager $em
 */
class AddTaskCommand extends ContainerAwareCommand
{
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em, TranslatorInterface $trans)
    {
        parent::__construct();

        $this->em = $em;
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('add:task');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        while (true) {
            $task = new Task();
            $task
                ->setType(TaskTypeEnum::SUBSCRIBE)
                ->setLink('https://vk.com/public138871416')
                ->setImageUrl('https://pp.userapi.com/c636616/v636616974/58fb9/u-H4NBDyRT4.jpg')
                ->setCost(15)
                ->setSocial(SocialEnum::VK)
                ->setStatus(TaskStatusEnum::ACTIVE)
                ->setText('Вечер в хату')
            ;

            $this->em->persist($task);
            $this->em->flush();
            sleep(5);
        }
    }
}
