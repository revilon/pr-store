<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

/**
 * Class DemonSocketCommand
 * @property string php
 * @property SwooleSocketCommand socket
 * @property string $console
 * @package AppBundle\Command
 */
class DemonSocketCommand extends ContainerAwareCommand
{
    private const ACTION_START = 'start';
    private const ACTION_STOP = 'stop';
    private const ACTION_RESTART = 'restart';
    private const ACTION_STATUS = 'status';

    /**
     * DemonSocketCommand constructor.
     * @param SwooleSocketCommand $socket
     * @param KernelInterface $kernel
     */
    public function __construct(SwooleSocketCommand $socket, KernelInterface $kernel)
    {
        parent::__construct();

        $this->socket = $socket;

        $phpBinaryFinder = new PhpExecutableFinder();
        $this->php = $phpBinaryFinder->find();

        $rootPath = $kernel->getRootDir();
        $this->console = sprintf('%s/../bin/console', $rootPath);
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $help = <<<TEXT
Usage: php bin/console socket:handle <action> [flag]
Commands:
start		    Start socket server in DAEMON mode.
stop		    Stop socket server.
                Use flag "force" to stop SIGKILL.
restart		    Restart socket server.
                Use flag "force" to stop SIGKILL.
status		    Get socket server status.
TEXT;
        $this
            ->setName('socket:handle')
            ->addArgument('action', InputArgument::REQUIRED, implode(
                '/',
                $this->getAvailableAction()
            ))
            ->addArgument(
                'flag',
                InputArgument::OPTIONAL,
                '"force" - if want execute danger command'
            )
            ->setHelp($help)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $action = $input->getArgument('action');
        $flag = $input->getArgument('flag');

        switch ($action) {
            case self::ACTION_START:
                $stdout = $this->start();
                $output->writeln($stdout);
                break;

            case self::ACTION_STOP:
                $signal = $flag === 'force' ? SIGKILL : SIGTERM;

                $stdout = $this->stop($signal);
                $output->writeln($stdout);
                break;

            case self::ACTION_RESTART:
                $signal = $flag === 'force' ? SIGKILL : SIGTERM;

                $stdout = $this->restart($signal);
                $output->writeln($stdout);
                break;

            case self::ACTION_STATUS:
                $stdout = $this->status();
                $output->writeln($stdout);
                break;

            default:
                throw new InvalidArgumentException(sprintf(
                    'Action "%s" not exist. Available %s.',
                    $action,
                    implode('/', $this->getAvailableAction())
                ));
        }
    }

    /**
     * @return string
     */
    private function start(): string
    {
        exec(sprintf(
            sprintf(
                '%s %s %s > /dev/null 2>&1 &',
                $this->php,
                $this->console,
                $this->socket->getName()
            )
        ), $output);

        if (!empty($output)) {
            dump($output);
        }

        return 'Socket run.';
    }

    /**
     * @param int $signal
     * @return string
     */
    private function stop(int $signal = 15): string
    {
        exec(sprintf(
            'ps axf | grep %s | grep -v grep | awk \'{print "kill -%s " $1}\' | sh',
            $this->socket->getName(),
            $signal
        ), $output);

        sleep(2);

        if (!empty($output)) {
            dump($output);
        }

        return $this->status();
    }

    /**
     * @param int $signal
     * @return string
     */
    private function restart(int $signal = 15): string
    {
        exec(sprintf(
            'ps axf | grep %s | grep -v grep | awk \'{print "kill -%s " $1}\' | sh',
            $this->socket->getName(),
            $signal
        ));

        sleep(2);

        exec(sprintf(
            'ps axf | grep %s | grep -v grep | awk \'{print "PID: " $1}\'',
            $this->socket->getName(),
            $signal
        ), $output);
        if (!empty($output)) {
            dump($output);
            return sprintf('Fail restart.');
        }

        $this->start();

        return sprintf('Socket restart. Current status: %s%s', PHP_EOL, $this->status());
    }

    /**
     * @return string
     */
    private function status(): string
    {
        exec(sprintf(
            'ps axf | grep %s | grep -v grep | awk \'{print "Socket run pid - " $1}\'',
            $this->socket->getName()
        ), $result);

        if (empty($result)) {
            return 'Socket down.';
        }

        $output[] = '=================================================================================================';
        $output[] = 'Status processes:';
        $output[] = implode(PHP_EOL, $result);

        $output[] = sprintf(
            'Current number of connections - %s.',
            $this->socket->getCountConnections()
        );
        $output[] = '=================================================================================================';

        return implode(PHP_EOL, $output);
    }

    /**
     * @return array
     */
    private function getAvailableAction(): array
    {
        $constantList = (new \ReflectionClass($this))->getConstants();

        $availableAction = [];
        foreach ($constantList as $key => $constant) {
            if (strpos($key, 'ACTION_') !== false) {
                $availableAction[] = $constant;
            }
        }

        return $availableAction;
    }
}
