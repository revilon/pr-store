<?php

namespace AppBundle\Command;

use AppBundle\Route\Router;
use PHPSocketIO\Socket;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Console\Input\InputArgument;
use Workerman\Worker;
use PHPSocketIO\SocketIO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * // отправить текущему сокету сформировавшему запрос (туда откуда пришла)
 * socket.emit('eventClient', "this is a test");
 * // отправить всем пользователям, включая отправителя
 * io.sockets.emit('eventClient', "this is a test");
 * // отправить всем, кроме отправителя
 * socket.broadcast.emit('eventClient', "this is a test");
 * // отправить всем клиентам в комнате (канале) 'game', кроме отправителя
 * socket.broadcast.to('game').emit('eventClient', 'nice game');
 * // отправить всем клиентам в комнате (канале) 'game', включая отправителя
 * io.sockets.in('game').emit('eventClient', 'cool game');
 * // отправить конкретному сокету, по socketid
 * io.sockets.socket(socketid).emit('eventClient', 'for your eyes only');
 *
 * Class BaseSocketCommand
 * @property Router router
 * @property SocketIO io
 * @property Logger logger
 * @package AppBundle\Command
 * @deprecated use SwooleSocketCommand
 */
class BaseSocketCommand extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure(): void
    {
        $help = <<<TEXT
Usage: php bin/console socket:handle <action> [mode]
Commands:
start		    Start worker in DEBUG mode.
                Use mode "d" to start in DAEMON mode.
stop		    Stop worker.
                Use mode "g" to stop gracefully.
restart		    Restart workers.
                Use mode "d" to start in DAEMON mode.
                Use mode "g" to stop gracefully.
reload		    Reload codes.
                Use mode "g" to reload gracefully.
status		    Get worker status.
                Use mode "d" to show live status.
connections	    Get worker connections.
TEXT;

        $this
            ->setName('io:socket:handle')
            ->addArgument('action', InputArgument::REQUIRED, 'start/stop/status...')
            ->addArgument('mode', InputArgument::OPTIONAL, '"d" - if want demon mode')
            ->setHelp($help)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        global $argv;

        $argv[1] = $input->getArgument('action');

        $this->router = $this->getContainer()->get(Router::class);
        $this->logger = $this->getContainer()->get('logger');

        switch ($input->getArgument('mode')) {
            case 'd':
                $argv[2] = '-d';
                $this->router->debug = false;
                break;
            case 'g':
                $argv[2] = '-g';
                break;
        }

        $context = [
            'ssl' => [
                'local_cert' => $this->getContainer()->getParameter('ssl_certificate'),
                'local_pk'   => $this->getContainer()->getParameter('ssl_private_key'),
                'verify_peer' => false,
            ]
        ];
        $this->io = new SocketIO(9001, $context);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        try {
            $this->io->on('connection', function (Socket $socket) {
                $this->router->setSocket($socket);

                $methodList = $this->router->getMethodList();

                foreach ($methodList as $method) {
                    $this->router->run($method);
                }

                $socket->on('disconnect', function () {
                    // handle disconnect
                });
            });

            Worker::runAll();
        } catch (\Exception $e) {
            $trace = [];
            foreach ($e->getTrace() as $item) {
                $trace[] = sprintf(
                    '%s:%s',
                    $item['file'],
                    $item['line']
                );
            }
            $this->logger->critical(json_encode([
                'error' => $e->getMessage(),
                'trace' => $trace,
            ]));

            if ($this->router->debug === true) {
                dump($e->getMessage(), $trace);
            }
        }
    }
}
