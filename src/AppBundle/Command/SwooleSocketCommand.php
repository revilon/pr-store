<?php

namespace AppBundle\Command;

use AppBundle\Dto\BaseQueryDto;
use AppBundle\Entity\Connection;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorException;
use AppBundle\Exception\SocketErrorException;
use AppBundle\HttpFoundation\Request;
use AppBundle\Route\Router;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Swoole\Http\Request as SocketRequest;
use Swoole\Http\Response;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SwooleSocketCommand
 * @package AppBundle\Command
 *
 * @property EntityManager em
 * @property \swoole_websocket_server $socket
 * @property Logger logger
 * @property Router router
 * @property BaseQueryDto baseQueryDto
 * @property DataCollectorTranslator trans
 * @property Request request
 *
 * Native command:
 * @start
 * php bin/console swoole:socket:handle > /dev/null 2>&1 &
 *
 * @start-debug
 * php bin/console swoole:socket:handle
 *
 * @status
 * ps aux |grep swoole
 *
 * @active-client-list
 * SELECT * FROM connection;
 *
 * @stop
 * ps aux |grep swoole
 * kill -15 [PID]
 */
class SwooleSocketCommand extends ContainerAwareCommand
{
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em, TranslatorInterface $trans)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->em = $em;
        $this->trans = $trans;
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('swoole:socket:handle');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->router = $this->getContainer()->get(Router::class);

        $this->socket = new Server('0.0.0.0', 9002, SWOOLE_PROCESS, SWOOLE_SOCK_TCP | SWOOLE_SSL);
        $this->socket->on('handshake', [$this, 'handshake']);
        $this->socket->set([
            'ssl_cert_file' => $this->getContainer()->getParameter('ssl_certificate'),
            'ssl_key_file'   => $this->getContainer()->getParameter('ssl_private_key'),
        ]);

        $this->em->getConnection()->executeQuery('TRUNCATE TABLE connection;');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->socket->on('message', function (Server $socket, Frame $frame) {
            try {
                $this->hydrateMySQLConnection();

                $this->updateSocketConnect($frame);

                $request = json_decode($frame->data, true);

                if (!\is_array($request)) {
                    throw new ApiErrorException(ApiErrorCodeEnum::QUERY_PARSE_ERROR);
                }
                $method = $request['method'];
                $data = $request['data'];
                $context = $request['context'] ?? null;
                $accept = $request['accept'] ?? null;

                $baseQueryDto = new BaseQueryDto();
                $baseQueryDto
                    ->setConnection($this->em->find(Connection::class, $frame->fd))
                    ->setMethod($method)
                    ->setQuery($data)
                    ->setSocket($socket)
                    ->setContext($context)
                    ->setAccept($accept)
                ;

                $this->router->run($baseQueryDto);
            } catch (SocketErrorException $exception) {
                $this->breakConnection($frame->fd);
            } catch (\Exception $exception) {
                $this->alarmReportError($exception, $frame->fd);
            }
        });

        $this->socket->on('close', function (Server $socket, $clientId) {
            $connection = $this->em->getReference(Connection::class, $clientId);

            $this->em->remove($connection);
            $this->em->flush();
        });

        $this->socket->on('open', function (Server $socket, $clientId) {
            // handle open (not work)
        });

        $this->socket->start();
    }

    /**
     * @param SocketRequest $swooleRequest
     * @param Response $response
     * @return bool
     */
    public function handshake(SocketRequest $swooleRequest, Response $response): bool
    {
        $secWebSocketKey = $swooleRequest->header['sec-websocket-key'];
        $patten = '#^[+/0-9A-Za-z]{21}[AQgw]==$#';

        if (0 === preg_match($patten, $secWebSocketKey) || 16 !== \strlen(base64_decode($secWebSocketKey))) {
            $response->end();
            return false;
        }

        $key = base64_encode(sha1(
            $swooleRequest->header['sec-websocket-key'] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11',
            true
        ));

        $headers = [
            'Upgrade' => 'websocket',
            'Connection' => 'Upgrade',
            'Sec-WebSocket-Accept' => $key,
            'Sec-WebSocket-Version' => '13',
        ];

        // WebSocket connection to 'ws://127.0.0.1:9502/'
        // failed: Error during WebSocket handshake:
        // Response must not include 'Sec-WebSocket-Protocol' header if not present in request: websocket
        if (isset($swooleRequest->header['sec-websocket-protocol'])) {
            $headers['Sec-WebSocket-Protocol'] = $swooleRequest->header['sec-websocket-protocol'];
        }

        foreach ($headers as $key => $val) {
            $response->header($key, $val);
        }

        $response->status(101);
        $response->end();

        $this->hydrateMySQLConnection();
        $this->requestInit($swooleRequest);
        $this->registrationConnect();

        return true;
    }

    /**
     * @param SocketRequest $swooleRequest
     * @return void
     */
    public function requestInit(SocketRequest $swooleRequest): void
    {
        $this->request = new Request($swooleRequest);
    }

    /**
     * @param \Exception $e
     * @param int $clientId
     */
    public function alarmReportError(\Exception $e, int $clientId): void
    {
        $code = (int)$e->getCode();
        $message = $this->trans->trans($e->getCode());

        if ($code === (int)$message) {
            $message = $e->getMessage();
        }

        $trace = $e->getTrace();

        foreach ($trace as $key => &$item) {
            if ($key === 'args') {
                $item['args'] = serialize($item);
            }
        }
        unset($item);

        $this->logger->critical(json_encode([
            'error' => $e->getMessage(),
            'trace' => $trace,
        ]));

        $data = [
            'status' => 'error',
            'method' => ApiErrorException::METHOD,
            'data' => [
                'code' => $e->getCode(),
                'description' => $message,
            ]
        ];

        if ($e instanceof ApiErrorException) {
            $data += ['parent_method' => $e->getParentMethod()];
        }

        $this->socket->push($clientId, json_encode($data));
    }

    /**
     * @return void
     */
    public function registrationConnect(): void
    {
        $connection = new Connection();

        $connection
            ->setId($this->request->getFd())
            ->setUserAgent($this->request->headers->get('User-Agent'))
            ->setIp($this->request->server->get('REMOTE_ADDR'))
            ->setPort($this->request->server->get('REMOTE_PORT'))
            ->setLocale($this->request->getPreferredLanguage())
        ;


        $this->em->persist($connection);
        $this->em->flush();
    }

    /**
     * @return void
     */
    public function hydrateMySQLConnection(): void
    {
        $this->logger->info(json_encode([
            'Type' => 'ping',
            'Test Mysql' => $this->em->getConnection()->ping(),
        ]));
        if ($this->em->getConnection()->ping() === false) {
            $this->logger->warn('MySQL server has gone away, connection restart.');
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
        }
    }

    /**
     * @param Frame $frame
     * @return void
     * @throws SocketErrorException
     */
    public function updateSocketConnect(Frame $frame): void
    {
        $connection = $this->em->find(Connection::class, $frame->fd);

        if ($connection === null) {
            throw new SocketErrorException(ApiErrorCodeEnum::SOCKET_CONNECTION_NOT_EXIST);
        }

        $request = json_decode($frame->data, true);
        if (!empty($request['access_token'])) {
            $connection->setAuth($request['access_token'] ?? null);
        }

        $connection->setUpdatedAt(new \DateTime());

        $this->em->persist($connection);
        $this->em->flush();
    }

    /**
     * @param int $clientId
     * @return void
     */
    public function breakConnection(int $clientId): void
    {
        $this->socket->close($clientId);
    }

    /**
     * @return int
     */
    public function getCountConnections(): int
    {
        $result = $this->em->getConnection()->executeQuery('SELECT count(*) FROM connection;')->fetch();

        return current(array_values($result));
    }
}
