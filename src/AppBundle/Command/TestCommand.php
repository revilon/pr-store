<?php

namespace AppBundle\Command;

use AppBundle\Controller\ApiController;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Enum\SocialEnum;
use AppBundle\Enum\TaskStatusEnum;
use AppBundle\Enum\TaskTypeEnum;
use AppBundle\Service\Task\TaskFormatter;
use AppBundle\Service\Task\TaskHandler;
use AppBundle\Service\Task\TaskQuery;
use AppBundle\Service\Task\TaskTotalQuery;
use AppBundle\Service\TaskTotal\TaskTotalFormatter;
use AppBundle\Service\TaskTotal\TaskTotalHandler;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class TestCommand
 * @package AppBundle\Command
 *
 * @property EntityManager $em
 * @property LoggerInterface logger
 * @property TranslatorInterface trans
 */
class TestCommand extends ContainerAwareCommand
{
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em, TranslatorInterface $trans)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->em = $em;
        $this->trans = $trans;
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('t');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $u = new User();
        $u->setEmail('asd@jj.ru');
//        $u->setUsername('asd@jj.ru');
        $u->setPassword('asjdhdsah');
        $u->setIp('54.88.222s.11');
        $this->em->persist($u);
        dd($u);
//        $validator = $this->getContainer()->get('validator');
//        $e = $validator->validate($u);
dd((string)$e);
        for ($i = 0; $i < 100; $i++) {
            $userRepository = $this->em->getRepository(User::class);
            $u[] = $userRepository->findOneBy(['token' => '$2y$10$dSrTcOv7oWY2HpRT8/oEyO13itZX8xIS2sUWW1lrIYN5sxLmmGV.C']);
        }
        dd($u);
//        $query = $this->getContainer()->get(TaskQuery::class);
//        $query->resolve([
//            'social' => strtolower(SocialEnum::INSTAGRAM),
//            'type' => strtolower(TaskTypeEnum::VOTE),
//            'limit' => 10,
//            'offset' => 0,
//        ]);
//        $handler = $this->getContainer()->get(TaskHandler::class);
//        $formatter = $this->getContainer()->get(TaskFormatter::class);
//        $data = $handler->handle($query);
//        $data = $formatter->format($data);

        $handler = $this->getContainer()->get(TaskTotalHandler::class);
        $formatter = $this->getContainer()->get(TaskTotalFormatter::class);
        $d = $handler->handle();
        $d = $formatter->format($d);
        dd($d);
//        dump($data);
//        if ($this->em->getConnection()->ping() === false) {
//            $this->em->getConnection()->close();
//            $this->em->getConnection()->connect();
//        }
    }
}
