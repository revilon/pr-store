<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use AppBundle\Enum\SocialEnum;
use AppBundle\Enum\TaskStatusEnum;
use AppBundle\Enum\TaskTypeEnum;
use AppBundle\Service\AuthorizationService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use function Swagger\scan;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class DefaultController
 * @property EntityManagerInterface em
 * @property AuthorizationService authorizationService
 * @property TranslatorInterface translator
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * DefaultController constructor.
     * @param EntityManagerInterface $em
     * @param AuthorizationService $authorizationService
     * @param TranslatorInterface $translator
     */
    public function __construct(
        EntityManagerInterface $em,
        AuthorizationService $authorizationService,
        TranslatorInterface $translator
    ) {
        $this->em = $em;
        $this->authorizationService = $authorizationService;
        $this->translator = $translator;
    }

    /**
     * @Route("/test", name="test")
     */
    public function indexsAction(Request $request)
    {
        dd($request->server);

        return $this->render('AppBundle::index.html.twig');
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle::index.html.twig');
    }

    /**
     * @Route("/main", name="main")
     * @param Request $request
     * @return Response
     */
    public function mainAction(Request $request): Response
    {
        try {
            $accessToken = $request->cookies->get('access_token');
            $this->authorizationService->check($accessToken);
        } catch (\Exception $exception) {
            $this->redirectToRoute('login');
        }

        $em = $this->container->get('doctrine.orm.entity_manager');
        $taskRepository = $em->getRepository(Task::class);
        $queryBuilder = $taskRepository->createQueryBuilder('t');
        $taskTotal = $queryBuilder
            ->select('t.social')
            ->addSelect('t.type')
            ->addSelect(
                $queryBuilder->expr()->count('t.type') . 'as count'
            )
            ->where('t.status = :status')
            ->groupBy('t.social, t.type')
            ->setParameter('status', TaskStatusEnum::ACTIVE)
            ->getQuery()->execute()
        ;
        $socialList = [];
        /** @var array $taskTotal */
        foreach ($taskTotal as $item) {
            $socialList[$item['social']][] = [
                'social_id' => $item['social'],
                'type_id' => $item['type'],
                'social' => $this->translator->trans($item['social']),
                'type' => $this->translator->trans($item['type']),
                'count' => $item['count'],
            ];
        }

        $limit = 10;
        $taskList = $taskRepository->findBy(
            [
                'status' => TaskStatusEnum::ACTIVE,
                'type' => TaskTypeEnum::LIKE,
                'social' => SocialEnum::INSTAGRAM,
            ],
            ['cost' => 'DESC'],
            $limit + 1
        );

        $taskDataList = [];

        /** @var Task[] $taskList */
        foreach ($taskList as $task) {
            $taskDataList[] = [
                'id' => $task->getId(),
                'type_id' => $task->getType(),
                'social_id' => $task->getSocial(),
                'text' => $task->getText(),
                'link' => $task->getLink(),
                'image_url' => $task->getImageUrl(),
                'cost' => $task->getCost(),
                'name' => $task->getName(),
            ];
        }

        $isHasMore = false;
        if (count($taskList) > $limit) {
            $isHasMore = true;
            array_pop($taskList);
        }

        return $this->render('@View/gainMoney/main.html.twig', [
            'social_list' => $socialList,
            'task_list' => $taskDataList,
            'has_more' => $isHasMore,
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        return $this->render('ViewBundle::login.html.twig');
    }

    /**
     * @Route("/registration", name="registration")
     */
    public function registrationAction(Request $request)
    {
        return $this->render('ViewBundle::registration.html.twig');
    }

    /**
     * @Route("/api/doc/json", name="api_doc_json")
     */
    public function docJsonAction(): Response
    {
        $srcDir = $this->get('kernel')->getRootDir() . '/../src';
        $swagger = scan($srcDir);

        return $this->json($swagger);
    }

    /**
     * @Route("/api/doc/view", name="api_doc_view")
     */
    public function docViewAction(): Response
    {
        return $this->render('AppBundle::doc.html.twig');
    }
}
