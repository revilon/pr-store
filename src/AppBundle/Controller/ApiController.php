<?php

namespace AppBundle\Controller;

use AppBundle\Dto\BaseQueryDto;
use AppBundle\Exception\ApiErrorCheckExternalAuthorizationException;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorException;
use AppBundle\Exception\ApiErrorLoginException;
use AppBundle\Exception\ApiErrorRegistrationException;
use AppBundle\Exception\ApiErrorTaskTotalException;
use AppBundle\Service\AuthorizationService;
use AppBundle\Service\CheckCompleteTask\CheckCompleteTaskHandler;
use AppBundle\Service\CheckCompleteTask\CheckCompleteTaskQuery;
use AppBundle\Service\CheckExternalAuthorization\CheckExternalAuthorizationFormatter;
use AppBundle\Service\CheckExternalAuthorization\CheckExternalAuthorizationHandler;
use AppBundle\Service\CheckExternalAuthorization\CheckExternalAuthorizationQuery;
use AppBundle\Service\Login\LoginHandler;
use AppBundle\Service\Login\LoginQuery;
use AppBundle\Service\Task\TaskFormatter;
use AppBundle\Service\Task\TaskHandler;
use AppBundle\Service\Task\TaskQuery;
use AppBundle\Service\Registration\RegistrationHandler;
use AppBundle\Service\Registration\RegistrationQuery;
use AppBundle\Service\TaskTotal\TaskTotalFormatter;
use AppBundle\Service\TaskTotal\TaskTotalHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

/**
 * @SWG\Swagger(
 *     schemes={"wss"},
 *     host="pr-store.9ek.ru",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="PR-Store Api Doc",
 *         @SWG\Contact(
 *             email="revil-on@mail.ru"
 *         ),
 *     ),
 *     @SWG\ExternalDocumentation(
 *         url="https://gitlab.com/revilon/pr-store/wikis/home"
 *     ),
 *     @SWG\Definition(
 *          definition="ErrorModel",
 *          @SWG\Property(
 *              property="status",
 *              type="string",
 *              example="error"
 *          ),
 *          @SWG\Property(
 *              property="method",
 *              type="string",
 *              example="handler.error",
 *          ),
 *          @SWG\Property(
 *              property="parent_method",
 *              type="string",
 *              example="login",
 *          ),
 *          @SWG\Property(
 *              property="data",
 *              type="object",
 *              required={"code", "description"},
 *              @SWG\Property(
 *                  property="code",
 *                  type="integer",
 *              ),
 *              @SWG\Property(
 *                  property="description",
 *                  type="string"
 *              )
 *          )
 *     ),
 *     @SWG\Definition(
 *          definition="Meta",
 *          @SWG\Property(
 *              property="code",
 *              type="integer",
 *              example=200
 *          ),
 *          @SWG\Property(
 *              property="context",
 *              type="string",
 *              description="The string that was passed in the request"
 *          ),
 *          @SWG\Property(
 *              property="content_type",
 *              enum={"text/html", "application/json"},
 *              type="string",
 *              description="Type of response, taking into account the wishes of the header 'Accept'"
 *          ),
 *          @SWG\Property(
 *              property="limit",
 *              type="integer",
 *              description="Used when transferring lists"
 *          ),
 *          @SWG\Property(
 *              property="offset",
 *              type="integer",
 *              description="Used when transferring lists"
 *          ),
 *     ),
 *     @SWG\Parameter(
 *         name="context",
 *         required=false,
 *         in="path",
 *         type="string",
 *         description="Context of call, this string will be returning in response",
 *     ),
 *     @SWG\Parameter(
 *         name="accept",
 *         required=false,
 *         in="path",
 *         enum={"text/html", "application/json"},
 *         type="string",
 *         description="format requested content. Default='application/json'",
 *     ),
 *     @SWG\SecurityScheme(
 *          name="auth",
 *          securityDefinition="BaseAuthentication",
 *          type="apiKey",
 *          in="query"
 *     )
 * )
 *
 * Class ApiController
 * @package AppBundle\Controller
 */
class ApiController extends BaseController
{
    /**
     * @SWG\Get(
     *  path="/login",
     *  summary="Login exist user and get authorization token.",
     *  consumes={"application/json"},
     *  produces={"application/json"},
     *  tags={"Application"},
     *  @SWG\Parameter(
     *      name="email",
     *      required=true,
     *      in="path",
     *      description="User email",
     *      type="string",
     *  ),
     *  @SWG\Parameter(
     *      name="password",
     *      required=true,
     *      in="path",
     *      description="Password",
     *      type="string",
     *  ),
     *  @SWG\Parameter(ref="#/parameters/context"),
     *  @SWG\Response(
     *      response=200,
     *      description="Success login, take authorization token",
     *      @SWG\Schema(
     *          @SWG\Property(
     *              property="status",
     *              type="string",
     *              example="success"
     *          ),
     *          @SWG\Property(
     *              property="method",
     *              type="string",
     *              example="login",
     *          ),
     *          @SWG\Property(
     *              property="data",
     *              type="object",
     *              required={"token"},
     *              @SWG\Property(
     *                  property="token",
     *                  type="string",
     *                  example="$2y$10$hbHgg.vR1eeXQpnj2xcahOfhq29WkICerxk3JI09S/pcNHfwe6irC"
     *              )
     *          )
     *      )
     *  ),
     *  @SWG\Response(
     *      response="400",
     *      description="Invalid request params",
     *      @SWG\Schema(ref="#/definitions/ErrorModel"),
     *  )
     * )
     * @param BaseQueryDto $baseQuery
     * @return void
     * @throws ApiErrorException
     */
    public function loginAction(BaseQueryDto $baseQuery): void
    {
        /** @var Request $request */
        $handler = $this->container->get(LoginHandler::class);
        $query = $this->container->get(LoginQuery::class);

        try {
            $query->resolve($baseQuery->getQuery());

            $data = $handler->handle($query);
        } catch (ApiErrorLoginException $exception) {
            $exception = new ApiErrorException($exception->getCode());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        } catch (\Exception $exception) {
            $exception = new ApiErrorException(ApiErrorCodeEnum::UNEXPECTED_ERROR, $exception->getMessage());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        }

        $this->respond($baseQuery, $data);
    }

    /**
     * @SWG\Get(
     *  path="/registration",
     *  summary="Registration new user and get authorization token.",
     *  consumes={"application/json"},
     *  produces={"application/json"},
     *  tags={"Application"},
     *  @SWG\Parameter(
     *      name="email",
     *      required=true,
     *      in="path",
     *      description="Valid user email",
     *      type="string",
     *  ),
     *  @SWG\Parameter(
     *      name="password",
     *      minLength=6,
     *      maxLength=20,
     *      required=true,
     *      in="path",
     *      description="Password any 6-20 symbol",
     *      type="string",
     *  ),
     *  @SWG\Response(
     *       response=200,
     *       description="Success registration, take authorization token",
     *       @SWG\Schema(
     *          @SWG\Property(
     *              property="status",
     *              type="string",
     *              example="success"
     *          ),
     *          @SWG\Property(
     *              property="method",
     *              type="string",
     *              example="registration",
     *          ),
     *          @SWG\Property(
     *              property="data",
     *              type="object",
     *              required={"token"},
     *              @SWG\Property(
     *                  property="token",
     *                  type="string",
     *                  example="$2y$10$hbHgg.vR1eeXQpnj2xcahOfhq29WkICerxk3JI09S/pcNHfwe6irC"
     *              )
     *          )
     *       )
     *  ),
     *  @SWG\Parameter(ref="#/parameters/context"),
     *  @SWG\Response(
     *      response="400",
     *      description="Invalid request params",
     *      @SWG\Schema(ref="#/definitions/ErrorModel"),
     *  )
     * )
     * @param BaseQueryDto $baseQuery
     * @return void
     * @throws ApiErrorException
     */
    public function registrationAction(BaseQueryDto $baseQuery): void
    {
        // http://www.geoplugin.net/php.gp?ip=5.228.131.221
        $handler = $this->container->get(RegistrationHandler::class);
        $query = $this->container->get(RegistrationQuery::class);

        $query->setUserAgent($baseQuery->getConnection()->getUserAgent());
        $query->setIp($baseQuery->getConnection()->getIp());

        try {
            $query->resolve($baseQuery->getQuery());

            $data = $handler->handle($query);
        } catch (ApiErrorRegistrationException $exception) {
            $exception = new ApiErrorException($exception->getCode());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        } catch (\Exception $exception) {
            $exception = new ApiErrorException(ApiErrorCodeEnum::UNEXPECTED_ERROR, $exception->getMessage());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        }

        $this->respond($baseQuery, $data);
    }

    /**
     * @SWG\Get(
     *  path="/taskTotal",
     *  summary="Get total task",
     *  consumes={"application/json"},
     *  produces={"application/json"},
     *  tags={"Application"},
     *  @SWG\Response(
     *      response=200,
     *      description="Take total task all social network.",
     *      @SWG\Schema(
     *          @SWG\Property(
     *              property="status",
     *              type="string",
     *              example="success"
     *          ),
     *          @SWG\Property(
     *              property="method",
     *              type="string",
     *              example="taskTotal",
     *          ),
     *          @SWG\Property(
     *              property="data",
     *              type="object",
     *              required={"task_total_list"},
     *              @SWG\Property(
     *                  property="task_total_list",
     *                  type="array",
     *                  @SWG\Items(
     *                      @SWG\Property(
     *                          property="id",
     *                          type="string",
     *                          example="vk"
     *                      ),
     *                      @SWG\Property(
     *                          property="name",
     *                          type="string",
     *                          example="Вконтакте"
     *                      ),
     *                      @SWG\Property(
     *                          property="task_type",
     *                          type="object",
     *                          @SWG\Property(
     *                              property="comment",
     *                              type="integer",
     *                              example=22
     *                          ),
     *                          @SWG\Property(
     *                              property="like",
     *                              type="integer",
     *                              example=56
     *                          ),
     *                          @SWG\Property(
     *                              property="share",
     *                              type="integer",
     *                              example=12
     *                          ),
     *                          @SWG\Property(
     *                              property="subscribe",
     *                              type="integer",
     *                              example=32
     *                          ),
     *                          @SWG\Property(
     *                              property="view",
     *                              type="integer",
     *                              example=98
     *                          ),
     *                          @SWG\Property(
     *                              property="vote",
     *                              type="integer",
     *                              example=110
     *                          ),
     *                      )
     *                  )
     *              )
     *          )
     *      )
     *  ),
     *  @SWG\Parameter(ref="#/parameters/context"),
     *  @SWG\Response(
     *      response="400",
     *      description="Invalid request params",
     *      @SWG\Schema(ref="#/definitions/ErrorModel"),
     *  ),
     *  security={{"BaseAuthentication":{}}}
     * )
     * @param BaseQueryDto $baseQuery
     * @return void
     * @throws ApiErrorException
     */
    public function taskTotalAction(BaseQueryDto $baseQuery): void
    {
        $this->authorization->check($baseQuery->getConnection()->getAuth());

        $handler = $this->container->get(TaskTotalHandler::class);
        $formatter = $this->container->get(TaskTotalFormatter::class);

        try {
            $data = $handler->handle();
            $data = $formatter->format($data);
        } catch (ApiErrorTaskTotalException $exception) {
            $exception = new ApiErrorException($exception->getCode());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        } catch (\Exception $exception) {
            $exception = new ApiErrorException(ApiErrorCodeEnum::UNEXPECTED_ERROR, $exception->getMessage());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        }

        $this->respond($baseQuery, $data);
    }

    /**
     * @SWG\Get(
     *  path="/task",
     *  summary="Get task list by social or social+type. Support limit/offset.",
     *  consumes={"application/json"},
     *  produces={"application/json"},
     *  tags={"Application"},
     *  @SWG\Parameter(
     *      name="social",
     *      required=true,
     *      in="path",
     *      enum={"instagram", "vk", "facebook", "youtube", "twitter", "odnoklassniki"},
     *      description="Id social network",
     *      type="string"
     *  ),
     *  @SWG\Parameter(
     *      name="type",
     *      required=false,
     *      in="path",
     *      enum={"like", "share", "subscribe", "comment", "vote", "view"},
     *      description="Type task",
     *      type="string"
     *  ),
     *  @SWG\Parameter(
     *      name="limit",
     *      required=false,
     *      in="path",
     *      description="Max task",
     *      type="integer"
     *  ),
     *  @SWG\Parameter(
     *      name="offset",
     *      required=false,
     *      in="path",
     *      description="Skip task",
     *      type="integer"
     *  ),
     *  @SWG\Response(
     *       response=200,
     *       description="Success registration, take authorization token",
     *       @SWG\Schema(
     *          @SWG\Property(
     *              property="status",
     *              type="string",
     *              example="success"
     *          ),
     *          @SWG\Property(
     *              property="method",
     *              type="string",
     *              example="task",
     *          ),
     *          @SWG\Property(
     *              property="data",
     *              type="object",
     *              required={"task_list"},
     *              @SWG\Property(
     *                  property="task_list",
     *                  type="array",
     *                  @SWG\Items(
     *                      @SWG\Property(
     *                          property="cost",
     *                          type="integer",
     *                          example=13
     *                      ),
     *                      @SWG\Property(
     *                          property="id",
     *                          type="integer",
     *                          example=8740
     *                      ),
     *                      @SWG\Property(
     *                          property="image_url",
     *                          type="string",
     *                          example="https://cs5.pikabu.ru/post_img/2014/07/31/11/1406828003_99111015.jpg"
     *                      ),
     *                      @SWG\Property(
     *                          property="link",
     *                          type="string",
     *                          example="https://www.instagram.com/p/BhSY2S8hbil/"
     *                      ),
     *                      @SWG\Property(
     *                          property="text",
     *                          type="string",
     *                          example="Посмотреть"
     *                      ),
     *                      @SWG\Property(
     *                          property="name",
     *                          type="string",
     *                          example="Часы пасмурные"
     *                      ),
     *                  )
     *              )
     *          )
     *       )
     *  ),
     *  @SWG\Parameter(ref="#/parameters/context"),
     *  @SWG\Parameter(ref="#/parameters/accept"),
     *  @SWG\Response(
     *      response="400",
     *      description="Invalid request params",
     *      @SWG\Schema(ref="#/definitions/ErrorModel"),
     *  ),
     *  security={{"BaseAuthentication":{}}}
     * )
     * @param BaseQueryDto $baseQuery
     * @return void
     * @throws ApiErrorException
     */
    public function taskAction(BaseQueryDto $baseQuery): void
    {
        $this->authorization->check($baseQuery->getConnection()->getAuth());

        $query = $this->container->get(TaskQuery::class);
        $handler = $this->container->get(TaskHandler::class);
        $formatter = $this->container->get(TaskFormatter::class);

        try {
            $query->resolve($baseQuery->getQuery());

            [$taskList, $isHasMore] = $handler->handle($query);

            $data = $formatter->format($taskList, $isHasMore);

            $baseQuery->meta->limit = $query->getLimit();
            $baseQuery->meta->offset = $query->getOffset();
        } catch (ApiErrorLoginException $exception) {
            $exception = new ApiErrorException($exception->getCode());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        } catch (\Exception $exception) {
            $exception = new ApiErrorException(ApiErrorCodeEnum::UNEXPECTED_ERROR, $exception->getMessage());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        }

        $this->respond($baseQuery, $data, 'ViewBundle::selectTaskType.html.twig');
    }

    /**
     * @SWG\Get(
     *  path="/checkExternalAuthorization",
     *  summary="Check external authorization.",
     *  consumes={"application/json"},
     *  produces={"application/json"},
     *  tags={"Application"},
     *  @SWG\Parameter(
     *      name="social_id",
     *      required=true,
     *      in="path",
     *      enum={"instagram", "vk", "facebook", "youtube", "twitter", "odnoklassniki"},
     *      description="Id social",
     *      type="string"
     *  ),
     *  @SWG\Response(
     *       response=200,
     *       description="Success external authorization, take external_id and etc",
     *       @SWG\Schema(
     *          @SWG\Property(
     *              property="status",
     *              type="string",
     *              example="success"
     *          ),
     *          @SWG\Property(
     *              property="method",
     *              type="string",
     *              example="checkExternalAuthorization",
     *          ),
     *          @SWG\Property(
     *              property="data",
     *              type="object",
     *              required={"task_list"},
     *              @SWG\Property(
     *                  property="external_id",
     *                  type="string",
     *                  example="242067281675"
     *              ),
     *              @SWG\Property(
     *                  property="social_type",
     *                  type="string",
     *                  example="instagram"
     *              ),
     *          )
     *       )
     *  ),
     *  @SWG\Parameter(ref="#/parameters/context"),
     *  @SWG\Response(
     *      response="400",
     *      description="Invalid request params",
     *      @SWG\Schema(ref="#/definitions/ErrorModel"),
     *  ),
     *  security={{"BaseAuthentication":{}}}
     * )
     * @param BaseQueryDto $baseQuery
     * @return void
     * @throws ApiErrorException
     */
    public function checkExternalAuthorizationAction(BaseQueryDto $baseQuery): void
    {
        $user = $this->authorization->getUser($baseQuery->getConnection()->getAuth());

        $query = $this->container->get(CheckExternalAuthorizationQuery::class);
        $handler = $this->container->get(CheckExternalAuthorizationHandler::class);
        $formatter = $this->container->get(CheckExternalAuthorizationFormatter::class);

        try {
            $query->resolve(array_merge($baseQuery->getQuery(), [
                'user' => $user,
            ]));

            $data = $handler->handle($query);

            $data = $formatter->format($data);
        } catch (ApiErrorCheckExternalAuthorizationException $exception) {
            $exception = new ApiErrorException($exception->getCode());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        } catch (\Exception $exception) {
            $exception = new ApiErrorException(ApiErrorCodeEnum::UNEXPECTED_ERROR, $exception->getMessage());
            $exception->setParentMethod($baseQuery->getMethod());

            throw $exception;
        }

        $this->respond($baseQuery, $data);
    }

    /**
     * @SWG\Get(
     *  path="/checkCompleteTask",
     *  summary="Check performance task.",
     *  consumes={"application/json"},
     *  produces={"application/json"},
     *  tags={"Application"},
     *  @SWG\Parameter(
     *      name="task_id",
     *      required=true,
     *      in="path",
     *      description="Id task",
     *      type="integer"
     *  ),
     *  @SWG\Parameter(ref="#/parameters/context"),
     *  @SWG\Response(
     *      response="400",
     *      description="Invalid request params",
     *      @SWG\Schema(ref="#/definitions/ErrorModel"),
     *  ),
     *  security={{"BaseAuthentication":{}}}
     * )
     * @param BaseQueryDto $baseQuery
     * @return void
     * @throws ApiErrorLoginException
     */
    public function checkCompleteTaskAction(BaseQueryDto $baseQuery): void
    {
        $user = $this->authorization->getUser($baseQuery->getConnection()->getAuth());

        $query = $this->container->get(CheckCompleteTaskQuery::class);
        $query->resolve(array_merge($baseQuery->getQuery(), [
            'user_id' => $user->getId(),
        ]));

        $handler = $this->container->get(CheckCompleteTaskHandler::class);
        $handler->handle($query);
        dump($query);
    }
}
