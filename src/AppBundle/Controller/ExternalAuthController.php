<?php

namespace AppBundle\Controller;

use AppBundle\Dto\InstagramUserDto;
use AppBundle\Dto\VkUserDto;
use AppBundle\Entity\ExternalAuth;
use AppBundle\Enum\ExternalAuthStatusEnum;
use AppBundle\Enum\SocialEnum;
use AppBundle\Service\AuthorizationService;
use AppBundle\Service\InstagramAuthService;
use AppBundle\Service\VkAuthService;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

/**
 * Class ExternalAuthController
 * @property EntityManagerInterface em
 * @property AuthorizationService authorizationService
 * @property VkAuthService vkAuthService
 * @package AppBundle\Controller
 */
class ExternalAuthController extends Controller
{
    /**
     * DefaultController constructor.
     * @param EntityManagerInterface $em
     * @param AuthorizationService $authorizationService
     * @param VkAuthService $vkAuthService
     */
    public function __construct(
        EntityManagerInterface $em,
        AuthorizationService $authorizationService,
        VkAuthService $vkAuthService
    ) {
        $this->em = $em;
        $this->authorizationService = $authorizationService;
        $this->vkAuthService = $vkAuthService;
    }

    /**
     * https://pr-store.9ek.ru/auth/instagram?self_access_token=%242y%2410%24dSrTcOv7oWY2HpRT8%2FoEyO13itZX8xIS2sUWW1lrIYN5sxLmmGV.C&action=make_access_token
     * @Route("/auth/instagram", name="auth_instagram")
     * @param Request $request
     * @return Response
     */
    public function authInstagramAction(Request $request): Response
    {
        $user = $this->authorizationService->getUser($request->get('self_access_token'));

        $externalAuthRepository = $this->em->getRepository(ExternalAuth::class);

        $externalAuth = $externalAuthRepository->findOneBy([
            'social' => SocialEnum::INSTAGRAM,
            'user' => $user,
            'status' => ExternalAuthStatusEnum::ACTIVE,
        ]);

        if ($externalAuth) {
            return $this->render('AppBundle::ExternalAuthorization.html.twig', [
                'status' => 'error',
                'result' => sprintf('Your have already authorization with %s', SocialEnum::INSTAGRAM),
            ]);
        }

        $redirectUri = sprintf(
            'https://%s%s?%s',
            $request->getHttpHost(),
            $request->getPathInfo(),
            http_build_query([
                'self_access_token' => $user->getToken(),
            ])
        );

        $authUrl = 'https://www.instagram.com/oauth/authorize?';
        $authUrl .= http_build_query([
            'client_id' => $this->getParameter('instagram.client_id'),
            'redirect_uri' => $redirectUri,
            'response_type' => 'code',
        ]);

        if ($request->get('action') === 'make_access_token') {
            return $this->redirect($authUrl);
        }

        $code = $request->get('code');
        $error = $request->get('error');
        $errorReason = $request->get('error_reason');
        $errorDescription = $request->get('error_description');

        $result = 'Unexpected error';
        switch (true) {
            case $code !== null:
                $client = new Client();

                $response = $client->request('POST', 'https://api.instagram.com/oauth/access_token', [
                    'http_errors' => false,
                    'form_params' => [
                        'client_id' => $this->getParameter('instagram.client_id'),
                        'client_secret' => $this->getParameter('instagram.client_secret'),
                        'grant_type' => 'authorization_code',
                        'redirect_uri' => $redirectUri,
                        'code' => $code,
                    ]
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                break;
            case $error !== null:
            case $errorReason !== null:
            case $errorDescription !== null:
                $result = [
                    'error' => $error,
                    'error_reason' => $errorReason,
                    'error_description' => $errorDescription,
                ];
                break;
        }

        if (isset($result['access_token'])) {
            $instagramUser = new InstagramUserDto();

            $instagramUser->setAccessToken($result['access_token']);
            $instagramAuthService = $this->get(InstagramAuthService::class);

            $instagramAuthService->fetchUser($instagramUser);

            if (!$externalAuth instanceof ExternalAuth) {
                $externalAuth = (new ExternalAuth())
                    ->setSocial(SocialEnum::INSTAGRAM)
                    ->setStatus(ExternalAuthStatusEnum::ACTIVE)
                    ->setAccessToken($instagramUser->getAccessToken())
                    ->setExternalId($instagramUser->getId())
                    ->setUser($user)
                ;
            }

            $this->em->persist($externalAuth);
            $this->em->flush();

            return $this->render('AppBundle::ExternalAuthorization.html.twig', [
                'status' => 'success',
                'result' => ''
            ]);
        }

        return $this->json([
            'status' => 'error',
            'data' => $result,
        ]);
    }

    /**
     * https://pr-store.9ek.ru/auth/vk?self_access_token=%242y%2410%24dSrTcOv7oWY2HpRT8%2FoEyO13itZX8xIS2sUWW1lrIYN5sxLmmGV.C&action=make_access_token
     * @Route("/auth/vk", name="auth_vk")
     * @param Request $request
     * @return Response
     */
    public function authVkAction(Request $request): Response
    {
        $selfAccessToken = $request->get('self_access_token') ?? $selfAccessToken = $request->get('state');
        $user = $this->authorizationService->getUser($selfAccessToken);

        $externalAuthRepository = $this->em->getRepository(ExternalAuth::class);

        $externalAuth = $externalAuthRepository->findOneBy([
            'social' => SocialEnum::VK,
            'user' => $user,
            'status' => ExternalAuthStatusEnum::ACTIVE,
        ]);

        if ($externalAuth) {
            return $this->render('AppBundle::ExternalAuthorization.html.twig', [
                'status' => 'error',
                'result' => sprintf('Your have already authorization with %s', SocialEnum::VK),
            ]);
        }

        $redirectUri = sprintf(
            'https://%s%s',
            $request->getHttpHost(),
            $request->getPathInfo()
        );

        $oauth = new VKOAuth();
        $authUrl = $oauth->getAuthorizeUrl(
            VKOAuthResponseType::CODE,
            $this->getParameter('vk.client_id'),
            $redirectUri,
            VKOAuthDisplay::PAGE,
            [
                VKOAuthUserScope::WALL,
                VKOAuthUserScope::GROUPS,
                VKOAuthUserScope::OFFLINE,
                VKOAuthUserScope::EMAIL,
                VKOAuthUserScope::FRIENDS,
                VKOAuthUserScope::PHOTOS,
                VKOAuthUserScope::PAGES,
            ],
            $user->getToken()
        );


        if ($request->get('action') === 'make_access_token') {
            return $this->redirect($authUrl);
        }

        $code = $request->get('code');
        if ($code) {
            $response = $oauth->getAccessToken(
                $this->getParameter('vk.client_id'),
                $this->getParameter('vk.client_secret'),
                $redirectUri,
                $code
            );


            $vkUserDto = new VkUserDto();
            $vkUserDto->setEmail($response['email']);
            $vkUserDto->setAccessToken($response['access_token']);

            $this->vkAuthService->fetchUser($vkUserDto);

            if (!$externalAuth instanceof ExternalAuth) {
                $externalAuth = (new ExternalAuth())
                    ->setSocial(SocialEnum::VK)
                    ->setStatus(ExternalAuthStatusEnum::ACTIVE)
                    ->setAccessToken($vkUserDto->getAccessToken())
                    ->setExternalId($vkUserDto->getId())
                    ->setUser($user)
                    ->setExternalEmail($vkUserDto->getEmail())
                ;
            }

            $this->em->persist($externalAuth);
            $this->em->flush();

            return $this->render('AppBundle::ExternalAuthorization.html.twig', [
                'status' => 'success',
                'result' => '',
            ]);
        }

        return $this->json([
            'status' => 'error',
            'data' => 'Unexpected error',
        ]);
    }
}
