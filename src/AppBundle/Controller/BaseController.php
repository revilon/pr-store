<?php

namespace AppBundle\Controller;

use AppBundle\Enum\AcceptEnum;
use AppBundle\Dto\BaseQueryDto;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorException;
use AppBundle\Service\AuthorizationService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseController
 * @property AuthorizationService authorization
 * @package AppBundle\Controller
 */
class BaseController extends Controller
{
    /**
     * RegistrationHandler constructor.
     * @param ContainerInterface $container
     * @param AuthorizationService $authorizationService
     */
    public function __construct(
        ContainerInterface $container,
        AuthorizationService $authorizationService
    ) {
        $this->container = $container;
        $this->authorization = $authorizationService;
    }

    /**
     * @param BaseQueryDto $baseQuery
     * @param array $data
     * @param string|null $template
     * @throws ApiErrorException
     */
    protected function respond(BaseQueryDto $baseQuery, array $data, string $template = null): void
    {
        if ($baseQuery->getMethod() === null) {
            throw new ApiErrorException(ApiErrorCodeEnum::UNEXPECTED_ERROR, 'Method empty!');
        }

        $content = $this->makeContent($baseQuery, $data);

        switch ($baseQuery->getAccept()) {
            case AcceptEnum::HTML:
                $content['data'] = $this->renderView($template, $content['data']);
                break;
        }

        $baseQuery->getSocket()->push($baseQuery->getConnection()->getId(), json_encode($content));
    }

    /**
     * @param BaseQueryDto $baseQuery
     * @param array $data
     * @return array
     */
    private function makeContent(BaseQueryDto $baseQuery, array $data): array
    {
        $meta = [
            'code' => 200,
            'context' => $baseQuery->getContext(),
            'content_type' => $baseQuery->getAccept(),
            'limit' => $baseQuery->meta->limit,
            'offset' => $baseQuery->meta->offset,
        ];

        return [
            'status' => 'success',
            'method' => $baseQuery->getMethod(),
            'data' => $data,
            'meta' => $meta,
        ];
    }
}
