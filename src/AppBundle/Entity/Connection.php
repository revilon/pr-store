<?php

namespace AppBundle\Entity;

use AppBundle\Traits\AtTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="connection")
 * @ORM\HasLifecycleCallbacks
 */
class Connection implements EntityInterface
{
    use AtTime;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $userAgent;

    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     */
    private $auth;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $ip;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $port;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $locale;

    /**
     * @return null|integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Connection
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     * @return Connection
     */
    public function setUserAgent(string $userAgent): Connection
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAuth(): ?string
    {
        return $this->auth;
    }

    /**
     * @param null|string $auth
     * @return Connection
     */
    public function setAuth(?string $auth): Connection
    {
        $this->auth = $auth;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return Connection
     */
    public function setIp(string $ip): Connection
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @param int $port
     * @return Connection
     */
    public function setPort(int $port): Connection
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @param string $locale
     * @return Connection
     */
    public function setLocale(string $locale): Connection
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }
}
