<?php

namespace AppBundle\Entity;

use AppBundle\Traits\AtTime;
use AppBundle\Traits\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="external_auth",
 *     uniqueConstraints={
 *        @ORM\UniqueConstraint(name="external_auth_unique",
 *            columns={"social", "external_id", "user_id"})
 *    })
 * @ORM\HasLifecycleCallbacks
 */
class ExternalAuth implements EntityInterface
{
    use Identifier;
    use AtTime;

    /**
     * @var string
     * @ORM\Column(type="social_enum")
     */
    private $social;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $externalId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $accessToken;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="externalAuthList", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="external_auth_status_enum", nullable=false)
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, length=1024)
     */
    private $errorMessage;

    /**
     * @return string
     */
    public function getSocial(): string
    {
        return $this->social;
    }

    /**
     * @param string $social
     * @return ExternalAuth
     */
    public function setSocial(string $social): ExternalAuth
    {
        $this->social = $social;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     * @return ExternalAuth
     */
    public function setExternalId(string $externalId): ExternalAuth
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return ExternalAuth
     */
    public function setAccessToken(string $accessToken): ExternalAuth
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @param User $user
     * @return ExternalAuth
     */
    public function setUser(User $user): ExternalAuth
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param string $status
     * @return ExternalAuth
     */
    public function setStatus(string $status): ExternalAuth
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $errorMessage
     * @return ExternalAuth
     */
    public function setErrorMessage(string $errorMessage): ExternalAuth
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @param string $externalEmail
     * @return ExternalAuth
     */
    public function setExternalEmail(string $externalEmail): ExternalAuth
    {
        $this->externalEmail = $externalEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalEmail(): string
    {
        return $this->externalEmail;
    }
}
