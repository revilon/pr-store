<?php

namespace AppBundle\Entity;

use AppBundle\Enum\SocialEnum;
use AppBundle\Traits\AtTime;
use AppBundle\Traits\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="task")
 * @ORM\HasLifecycleCallbacks
 */
class Task implements EntityInterface
{
    use Identifier;
    use AtTime;

    /**
     * @var string
     * @ORM\Column(type="social_enum", nullable=false)
     */
    private $social;

    /**
     * @var string
     * @ORM\Column(type="task_type_enum", nullable=false)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $link;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $imageUrl;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $cost;

    /**
     * @var string
     * @ORM\Column(type="task_status_enum", nullable=false)
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $text;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @return string
     */
    public function getSocial(): string
    {
        return $this->social;
    }

    /**
     * @param string $social
     * @return Task
     */
    public function setSocial(string $social): Task
    {
        if (!\in_array($social, SocialEnum::getList(), true)) {
            throw new \InvalidArgumentException('Invalid status');
        }
        $this->social = $social;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Task
     */
    public function setType(string $type): Task
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Task
     */
    public function setLink(string $link): Task
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     * @return Task
     */
    public function setImageUrl(string $imageUrl): Task
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->cost;
    }

    /**
     * @param int $cost
     * @return Task
     */
    public function setCost(int $cost): Task
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Task
     */
    public function setStatus(string $status): Task
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param string $text
     * @return Task
     */
    public function setText(string $text): Task
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $name
     * @return Task
     */
    public function setName(string $name): Task
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
