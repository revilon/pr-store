<?php

namespace AppBundle\Entity;

use AppBundle\Traits\AtTime;
use AppBundle\Traits\Identifier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks
 */
class User implements EntityInterface, UserInterface, \Serializable
{
    use Identifier;
    use AtTime;

    public function __construct()
    {
        $this->roles === null ? $this->roles = ['ROLE_USER'] : null;
        $this->active === null ? $this->active = true : null;
        $this->externalAuthList = new ArrayCollection();
    }

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *      min = 6,
     *      max = 20,
     * )
     */
    private $plainPassword;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @ORM\Column(type="string", nullable=false)
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(name="user_agent", type="string", nullable=false)
     */
    private $userAgent;

    /**
     * @var string
     * @ORM\Column(name="token", type="string", nullable=false, unique=true)
     */
    private $token;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Ip
     * @ORM\Column(name="ip", type="string", nullable=false)
     */
    private $ip;

    /**
     * @var \DateTime
     * @ORM\Column(name="last_login_at", type="datetime", nullable=false)
     */
    private $lastLoginAt;

    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var array
     * @ORM\Column(name="roles", type="array")
     */
    private $roles;

    /**
     * @var ExternalAuth[]
     * @ORM\OneToMany(targetEntity="ExternalAuth", mappedBy="user", cascade={"persist"})
     */
    private $externalAuthList;

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $userAgent
     * @return User
     */
    public function setUserAgent(string $userAgent): User
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @param string $token
     * @return User
     */
    public function setToken(string $token): User
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $ip
     * @return User
     */
    public function setIp(string $ip): User
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param \DateTime $lastLoginAt
     * @return User
     */
    public function setLastLoginAt(\DateTime $lastLoginAt): User
    {
        $this->lastLoginAt = $lastLoginAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastLoginAt(): \DateTime
    {
        return $this->lastLoginAt;
    }

    /**
     * @inheritdoc
     */
    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function unserialize($serialized)
    {
        [
            $this->id,
            $this->email,
            $this->password,
        ] = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return User
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword(string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    /**
     * @param ExternalAuth $externalAuth
     * @return User
     */
    public function addExternalAuth(ExternalAuth $externalAuth): User
    {
        $this->externalAuthList[] = $externalAuth;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getExternalAuthList(): ArrayCollection
    {
        return $this->externalAuthList;
    }
}
