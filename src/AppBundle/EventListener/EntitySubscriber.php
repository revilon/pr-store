<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EntityInterface;
use AppBundle\Validation\ValidatorService;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * Class EntitySubscriber
 * @property ValidatorService validator
 * @package AppBundle\EventListener
 */
class EntitySubscriber implements EventSubscriber
{
    public function __construct(ValidatorService $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            'prePersist',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->index($args);
    }

    /**
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function index(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof EntityInterface) {
            $this->validator->valid($entity);
        }
    }
}
