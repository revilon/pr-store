<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 28.11.2017
 * Time: 22:07
 */

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait Identifier
 * @package AppBundle\Traits
 */
trait Identifier
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @return null|integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}
