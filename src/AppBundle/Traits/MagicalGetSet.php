<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class MagicalGetSet
 * @package AppBundle\Traits
 * @ORM\HasLifecycleCallbacks
 */
trait MagicalGetSet
{
    /**
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (method_exists($this, $method)) {
            return \call_user_func_array([$this, $method], $parameters);
        }

        if (strpos($method, 'set') === 0) {
            $propertyCamelCase = substr($method, 3);

            $property = preg_replace_callback('/[A-Z]/', function ($matches) use ($propertyCamelCase) {
                static $matchCount = 0;

                if (strpos($propertyCamelCase, $matches[0]) === 0 && $matchCount === 0) {
                    $matchCount++;
                    return strtolower($matches[0]);
                }

                $matchCount++;
                return '_' . strtolower($matches[0]);
            }, $propertyCamelCase);

            $this->$property = current($parameters);

            return $this;
        }
        if (strpos($method, 'get') === 0) {
            $propertyCamelCase = substr($method, 3);

            $property = preg_replace_callback('/[A-Z]/', function ($matches) use ($propertyCamelCase) {
                static $matchCount = 0;

                if (strpos($propertyCamelCase, $matches[0]) === 0 && $matchCount === 0) {
                    $matchCount++;
                    return strtolower($matches[0]);
                }

                $matchCount++;
                return '_' . strtolower($matches[0]);
            }, $propertyCamelCase);

            return $this->$property;
        }

        return user_error(sprintf('undefined method "%s"', $method));
    }
}
