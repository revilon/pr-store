<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Enum\SocialEnum;
use AppBundle\Enum\TaskStatusEnum;
use AppBundle\Enum\TaskTypeEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\DataCollectorTranslator;

/**
 * Class AppFixtures
 * @property ContainerInterface container
 * @property DataCollectorTranslator trans
 * @property UserPasswordEncoderInterface passwordEncoder
 * @package AppBundle\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * AppFixtures constructor.
     * @param ContainerInterface $container
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->container = $container;
        $this->trans = $container->get('translator');
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        array_map([$manager, 'persist'], $this->user());
        array_map([$manager, 'persist'], $this->taskInstagram());
        array_map([$manager, 'persist'], $this->taskVk(TaskTypeEnum::COMMENT, 1));
        array_map([$manager, 'persist'], $this->taskVk(TaskTypeEnum::SHARE, 5));
        array_map([$manager, 'persist'], $this->taskVk(TaskTypeEnum::VOTE, 10));
        array_map([$manager, 'persist'], $this->taskVk(TaskTypeEnum::SUBSCRIBE, 20));
        array_map([$manager, 'persist'], $this->taskVk(TaskTypeEnum::VIEW, 35));
        array_map([$manager, 'persist'], $this->taskVk(TaskTypeEnum::LIKE, 24));

        $manager->flush();
    }

    /**
     * @return User[]
     */
    private function user(): array
    {
        $userList = [];

        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setIp(sprintf(
                '%s.%s.%s.%s',
                random_int(1, 255),
                random_int(1, 255),
                random_int(1, 255),
                random_int(1, 255)
            ));
            $user->setLastLoginAt(new \DateTime(sprintf('-%s days', random_int(1, 5))));
            $user->setUserAgent(
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 ' .
                '(KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
            );
            if ($i === 0) {
                $user->setToken('$2y$10$dSrTcOv7oWY2HpRT8/oEyO13itZX8xIS2sUWW1lrIYN5sxLmmGV.C');
            } else {
                $user->setToken(sprintf('$2y$10$dSrTcOv7oWY2HpRT8/oEyO13itZX8xIS2sUWW1lrIYN5sxLmmGV.C%s', $i));
            }

            $password = 'keklol';
            $user->setPlainPassword($password);
            $password = $this->passwordEncoder->encodePassword($user, $password);
            $user->setPassword($password);

            $user->setEmail(sprintf('test%s@mail.ru', $i));
            $user->setCreatedAt(new \DateTime(sprintf('2018-02-0%s 18:30:00', random_int(1, 9))));
            $user->setUpdatedAt(new \DateTime(sprintf('-%s days', random_int(5, 20))));

            $userList[] = $user;
        }

        return $userList;
    }

    /**
     * @return Task[]
     */
    private function taskInstagram(): array
    {
        $taskList = [];

        $contentList = [
            [
                'link' => 'https://www.instagram.com/p/Bh8oF0dh6aA/',
                'imageUrl' => 'https://scontent-arn2-1.cdninstagram.com/vp/d6e96e02a761349e50003d028fc41a00/5B9BC1BC/' .
                    't51.2885-15/s640x640/sh0.08/e35/30590951_1705566936147640_2107469061200805888_n.jpg',
            ],
            [
                'link' => 'https://www.instagram.com/p/BiCuN6DDto0/',
                'imageUrl' => 'https://scontent-arn2-1.cdninstagram.com/vp/dff586341060cb3c95d14993cd4f50bc/5B9B56A1/' .
                    't51.2885-15/sh0.08/e35/p640x640/30603631_398631580611742_4505345183683969024_n.jpg',
            ],
            [
                'link' => 'https://www.instagram.com/p/Bh-m4X5jVQ2/',
                'imageUrl' => 'https://scontent-arn2-1.cdninstagram.com/vp/afdf893d2948e9c883c6f7f87206c296/5B67748F/' .
                    't51.2885-15/sh0.08/e35/p640x640/30829376_1634559183323441_4505402207964758016_n.jpg',
            ],
            [
                'link' => 'https://www.instagram.com/p/BhSY2S8hbil/',
                'imageUrl' => 'https://scontent-arn2-1.cdninstagram.com/vp/54ae3cea1a879fbe8ed3a5d3a69c6f69/5B5F0364/' .
                    't51.2885-15/s640x640/sh0.08/e35/29740536_376160122883712_8412729103270019072_n.jpg',
            ]
        ];

        for ($i = 0; $i < 40; $i++) {
            $task = new Task();

            $contentKey = array_rand($contentList);

            switch (true) {
                case $i >= 0 && $i < 20:
                    $task->setStatus(TaskStatusEnum::ACTIVE);
                    break;
                case $i >= 20 && $i < 25:
                    $task->setStatus(TaskStatusEnum::NEW);
                    break;
                case $i >= 25 && $i < 30:
                    $task->setStatus(TaskStatusEnum::ABORT);
                    break;
                case $i >= 30 && $i < 35:
                    $task->setStatus(TaskStatusEnum::COMPLETE);
                    break;
                case $i >= 35 && $i < 40:
                    $task->setStatus(TaskStatusEnum::PAUSE);
                    break;
            }

            $task->setSocial(SocialEnum::INSTAGRAM);

            $type = array_rand(TaskTypeEnum::getListCombine());
            $task->setType($type);

            $task->setLink($contentList[$contentKey]['link']);
            $task->setImageUrl($contentList[$contentKey]['imageUrl']);

            $task->setText($this->trans->trans($type));
            $task->setName('Часы пасмурные');
            $task->setCost(random_int(3, 15));
            $task->setCreatedAt(new \DateTime(sprintf('2018-02-0%s 18:30:00', random_int(1, 9))));
            $task->setUpdatedAt(new \DateTime(sprintf('-%s days', random_int(5, 20))));

            $taskList[] = $task;
        }

        return $taskList;
    }

    /**
     * @param string $type
     * @param int $count
     * @return Task[]
     */
    public function taskVk(string $type, int $count): array
    {
        $taskList = [];

        $contentList = [
            [
                'link' => 'https://vk.com/prankotadotcom',
                'imageUrl' => 'https://pp.userapi.com/c629500/v629500137/2d67d/8DLEIj6KnaQ.jpg',
            ],
            [
                'link' => 'https://vk.com/public138871416',
                'imageUrl' => 'https://pp.userapi.com/c636616/v636616974/58fb9/u-H4NBDyRT4.jpg',
            ],
            [
                'link' => 'https://vk.com/21jqofa',
                'imageUrl' => 'https://pp.userapi.com/c613419/v613419645/1e380/her6pbgqreQ.jpg',
            ],
            [
                'link' => 'https://vk.com/xynta0',
                'imageUrl' => 'https://pp.userapi.com/c865/g30035753/e_874ee040.jpg',
            ]
        ];

        for ($i = 0; $i < $count; $i++) {
            $task = new Task();

            $contentKey = array_rand($contentList);

            $task->setStatus(TaskStatusEnum::ACTIVE);
            $task->setSocial(SocialEnum::VK);

            $task->setType($type);

            $task->setLink($contentList[$contentKey]['link']);
            $task->setImageUrl($contentList[$contentKey]['imageUrl']);

            $task->setText($this->trans->trans($type));
            $task->setName('Часы пасмурные');
            $task->setCost(random_int(3, 15));
            $task->setCreatedAt(new \DateTime(sprintf('2018-02-0%s 18:30:00', random_int(1, 9))));
            $task->setUpdatedAt(new \DateTime(sprintf('-%s days', random_int(5, 20))));

            $taskList[] = $task;
        }

        return $taskList;
    }
}
