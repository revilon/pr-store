<?php

namespace AppBundle\Dto;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InstagramUserDto
 * @package AppBundle\Dto
 *
 * @method string getId()
 * @method string getUsername()
 * @method string getProfilePictureUrl()
 * @method array getFullName()
 * @method string getBio()
 * @method string getWebsite()
 * @method string getBusinessStatus()
 * @method string getMedia()
 * @method string getFollows()
 * @method string getFollowedBy()
 */
class InstagramUserDto extends AbstractDto
{
    /** @var string */
    private $accessToken;

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setDefined([
            'id',
            'username',
            'profile_picture_url',
            'full_name',
            'bio',
            'website',
            'is_business',
            'media',
            'follows',
            'followed_by',
        ]);

        $options->setDefaults([
            'id' => '',
            'username' => '',
            'profile_picture_url' => '',
            'full_name' => '',
            'bio' => '',
            'website' => '',
            'is_business' => '',
            'media' => '',
            'follows' => '',
            'followed_by' => '',
        ]);
    }

    /**
     * @param string $accessToken
     * @return InstagramUserDto
     */
    public function setAccessToken(string $accessToken): InstagramUserDto
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }
}
