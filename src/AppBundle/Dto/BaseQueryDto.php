<?php

namespace AppBundle\Dto;

use AppBundle\Entity\Connection;
use AppBundle\HttpFoundation\Request;
use Swoole\WebSocket\Server;

/**
 * Class BaseQueryDto
 * @package AppBundle\Dto
 */
class BaseQueryDto
{
    /** @var string */
    private $context;

    /** @var string */
    private $accept;

    /** @var Server */
    private $socket;

    /** @var array */
    private $query;

    /** @var Connection */
    private $connection;

    /** @var string */
    private $method;

    /** @var Request */
    private $request;

    /** @var QueryMetaDto */
    public $meta;

    public function __construct()
    {
        $this->meta = new QueryMetaDto();
    }

    /**
     * @return Server
     */
    public function getSocket(): Server
    {
        return $this->socket;
    }

    /**
     * @param Server $socket
     * @return BaseQueryDto
     */
    public function setSocket(Server $socket): BaseQueryDto
    {
        $this->socket = $socket;
        return $this;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array $query
     * @return BaseQueryDto
     */
    public function setQuery(array $query): BaseQueryDto
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @param string $method
     * @return BaseQueryDto
     */
    public function setMethod(string $method): BaseQueryDto
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param Connection $connection
     * @return BaseQueryDto
     */
    public function setConnection(Connection $connection): BaseQueryDto
    {
        $this->connection = $connection;
        return $this;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }

    /**
     * @param Request $request
     * @return BaseQueryDto
     */
    public function setRequest(Request $request): BaseQueryDto
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param null|string $context
     * @return BaseQueryDto
     */
    public function setContext(?string $context): BaseQueryDto
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getContext(): ?string
    {
        return $this->context;
    }

    /**
     * @param null|string $accept
     * @return BaseQueryDto
     */
    public function setAccept(?string $accept): BaseQueryDto
    {
        $this->accept = $accept;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAccept(): ?string
    {
        return $this->accept;
    }
}
