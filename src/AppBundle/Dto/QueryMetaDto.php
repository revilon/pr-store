<?php

namespace AppBundle\Dto;

class QueryMetaDto
{
    /** @var int */
    public $limit;

    /** @var int */
    public $offset;
}
