<?php

namespace AppBundle\Dto;

use AppBundle\Traits\MagicalGetSet;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractRequest
 * @package AppBundle\Dto
 */
abstract class AbstractDto
{
    use MagicalGetSet;

    /**
     * AbstractRequest constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            $this->resolve($data);
        }
    }

    /**
     * @param array $data
     * @return void
     */
    public function resolve(array $data): void
    {
        $options = new OptionsResolver();
        $this->configureOptions($options);

        $data = $options->resolve($data);

        foreach ($options->getDefinedOptions() as $prop) {
            $this->$prop = $data[$prop];
        }
    }

    /**
     * @param OptionsResolver $options
     * @return void
     */
    abstract protected function configureOptions(OptionsResolver $options): void;
}
