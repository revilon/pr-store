<?php

namespace AppBundle\Dto;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InstagramUserDto
 * @package AppBundle\Dto
 *
 * @method int getId()
 * @method string getFullName()
 * @method int getSex()
 * @method string getNickname()
 * @method string getDomain()
 * @method string getBdate()
 * @method string getCity()
 * @method string getCountry()
 * @method int getTimezone()
 * @method string getPhotoMaxOrig()
 * @method int getPhotoId()
 * @method int getHasPhoto()
 * @method int getHasMobile()
 * @method int getOnline()
 * @method int getWallComments()
 * @method int getCanPost()
 * @method int getCanSeeAllPosts()
 * @method int getCanSeeAudio()
 * @method int getCanWritePrivateMessage()
 * @method int getCanSendFriendRequest()
 * @method string getMobilePhone()
 * @method string getHomePhone()
 * @method string getFacebook()
 * @method string getFacebookName()
 * @method string getInstagram()
 * @method string getSite()
 * @method string getStatusAudio()
 * @method string getStatus()
 * @method int getLastSeenUnix()
 * @method int getFollowersCount()
 */
class VkUserDto extends AbstractDto
{
    /** @var string */
    private $accessToken;

    /** @var string */
    private $email;

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setDefined([
            'id',
            'full_name',
            'sex',
            'nickname',
            'domain',
            'bdate',
            'city',
            'country',
            'timezone',
            'photo_max_orig',
            'photo_id',
            'has_photo',
            'has_mobile',
            'online',
            'wall_comments',
            'can_post',
            'can_see_all_posts',
            'can_see_audio',
            'can_write_private_message',
            'can_send_friend_request',
            'mobile_phone',
            'home_phone',
            'facebook',
            'facebook_name',
            'instagram',
            'site',
            'status_audio',
            'status',
            'last_seen_unix',
            'followers_count',
        ]);

        $options->setDefaults([
            'id' => '',
            'full_name' => '',
            'sex' => '',
            'nickname' => '',
            'domain' => '',
            'bdate' => '',
            'city' => '',
            'country' => '',
            'timezone' => '',
            'photo_max_orig' => '',
            'photo_id' => '',
            'has_photo' => '',
            'has_mobile' => '',
            'online' => '',
            'wall_comments' => '',
            'can_post' => '',
            'can_see_all_posts' => '',
            'can_see_audio' => '',
            'can_write_private_message' => '',
            'can_send_friend_request' => '',
            'mobile_phone' => '',
            'home_phone' => '',
            'facebook' => '',
            'facebook_name' => '',
            'instagram' => '',
            'site' => '',
            'status_audio' => '',
            'status' => '',
            'last_seen_unix' => '',
            'followers_count' => '',
        ]);
    }

    /**
     * @param string $accessToken
     * @return VkUserDto
     */
    public function setAccessToken(string $accessToken): VkUserDto
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return VkUserDto
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }
}
