<?php

namespace AppBundle\Route;

use AppBundle\Controller\ApiController;
use AppBundle\Dto\BaseQueryDto;
use AppBundle\Exception\ApiErrorException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Router
 * @package AppBundle\Route
 */
class Router
{
    /** @var ContainerInterface */
    protected $container;

    /**
     * MethodRouter constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->trans = $container->get('translator');
        $this->logger = $this->container->get('logger');
    }

    /**
     * @return array
     */
    public function getMethodList(): array
    {
        $controller = $this->container->get(ApiController::class);

        $methodList = get_class_methods($controller);

        foreach ($methodList as $key => $method) {
            if ($method === '__construct') {
                unset($methodList[$key]);
                continue;
            }

            $methodList[$key] = substr($method, 0, -6);
        }

        return $methodList;
    }

    /**
     * @param BaseQueryDto $baseQueryDto
     * @return void
     */
    public function run(BaseQueryDto $baseQueryDto): void
    {
        $this->route($baseQueryDto);
    }

    /**
     * @param BaseQueryDto $baseQuery
     * @return void
     * @throws ApiErrorException
     */
    public function route(BaseQueryDto $baseQuery): void
    {
        $controller = $this->container->get(ApiController::class);

        $action = sprintf('%sAction', $baseQuery->getMethod());

        $controller->$action($baseQuery);
    }
}
