import React from 'react'
import PropTypes from 'prop-types'
import styled, {css} from 'styled-components'
import {ifProp} from 'styled-tools'

const items = ({items}) => `${40 * items}px`
const SecitonWrapperOpen = ({open}) => open && css`
  button {
  &:before, &:after {
      height: 14px;
    }
  &:before {
      transform: translate(0%, -50%) rotate(45deg);
    }
  &:after {
      transform: translate(0%, -50%) rotate(-45deg);
    }
  }
  ${Articlewrap} {
    height: ${items};
    border-bottom: 1px solid #dadada;
  }
`
const HeaderMenuItemStyles = ({mobileHeader}) => mobileHeader && css`
  position: absolute;
  width: 100%;
  left: 0;
`
const SectionWrapperLastItem = ({lastItem}) => lastItem && css`
  margin-bottom: 200px;
`

const Articlewrap = styled.div`
  height: 0;
  overflow: hidden;
  transition: all .2s ease-in;
  ${HeaderMenuItemStyles};
`
const AccordionButton = styled.button`
    position: relative;
    outline: 0;
    border: 0;
    margin-left: auto;
    background: none;
    text-indent: -9999%;
    pointer-events: none;
    &:before {
      content: '';
      display: block;
      position: absolute;
      height: 12px;
      width: 4px;
      border-radius: .3em;
      background: ${ifProp('mobileHeader', '#000', '#6bddff')};
      transform-origin: 50%;
      top: 50%;
      left: 50%;
      transition: all .25s ease-in-out;
      transform: translate(75%, -50%) rotate(45deg);
    }
    &:after {
      content: '';
      display: block;
      position: absolute;
      height: 12px;
      width: 4px;
      border-radius: .3em;
      background: ${ifProp('mobileHeader', '#000', '#6bddff')};
      transform-origin: 50%;
      top: 50%;
      left: 50%;
      transition: all .25s ease-in-out;
      transform: translate(-75%, -50%) rotate(-45deg);
    }
`
const SectionWrapper = styled.div`
  width: ${ifProp('fluid', '100%', '260px')};
  transition: width .25s ease-in-out;
  flex: ${ifProp('fluid', '1 1 auto', 'none')};
  border-radius: unset;
  ${SecitonWrapperOpen};
  ${SectionWrapperLastItem};
`
const Sectionhead = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  padding: 1em;
  padding-right: 2.1em;
  border-bottom: ${ifProp('mobileHeader', 'none', '1px solid #6bddff')};
  height: 30px;
`
const Article = styled.div`
  color: #333;
  line-height: 1.3;
`
const Title = styled.div`
  display: inline-block;
  text-transform: capitalize;
  font-size: 14px;
`

class Accordion extends React.Component {
  constructor(props) {
    super(props)
    this.toggleAccordion = this.toggleAccordion.bind(this)
  }
  
  static propTypes = {
    children: PropTypes.any,
    title: PropTypes.any,
    items: PropTypes.number,
    icon: PropTypes.any,
    fluid: PropTypes.bool,
    lastItem: PropTypes.bool,
    mobileHeader: PropTypes.bool
  }

  state = {
    open: false,
  }
  
  toggleAccordion() {
    this.setState({
      ...this.state,
      open: !this.state.open,
    })
  }
  
  render() {
    const {children, title, items, icon, fluid, lastItem, mobileHeader} = this.props
    const {open} = this.state
    
    return (
      <SectionWrapper open={open} items={items} fluid={fluid} lastItem={lastItem} onClick={this.toggleAccordion}>
        <Sectionhead mobileHeader={mobileHeader}>
          {icon}
          <Title>{title}</Title>
          <AccordionButton mobileHeader={mobileHeader}/>
        </Sectionhead>
        <Articlewrap items={items} mobileHeader={mobileHeader}>
          <Article>
            {children}
          </Article>
        </Articlewrap>
      </SectionWrapper>
    )
  }
}

export default Accordion
