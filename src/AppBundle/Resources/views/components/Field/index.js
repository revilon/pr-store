import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Input from '../Input/index'
// import { Select } from 'molucules'
// import DateInput from '../DateInput'
// import FileInput from '../FileInput'
// import Wysiwyg from '../Wysiwyg'

const marginBottom = ({ type, bottomOffset }) =>
  typeof bottomOffset === 'number'
    ? bottomOffset
    : type === 'checkbox' ? 0.5 : 1
// eslint-disable-next-line
const Error = styled.div`
  margin: 0.5rem 0 0;
`
// eslint-disable-next-line
const FieldAsterisk = styled.sup`
  display: inline-block;
  vertical-align: middle;
  margin-top: 8px;
  margin-left: 2px;
  font-size: 14px;
  color: #f00;
`
const Wrapper = styled.div`
  flex: ${({ flex }) => flex};
  margin-bottom: ${marginBottom}rem;
  min-width: ${({ minWidth }) => (minWidth ? minWidth + 'px' : 'auto')};
  &:last-child {
    margin-bottom: 0;
  }
  input[type='checkbox'],
  input[type='radio'] {
    margin-right: 0.5rem;
  }
  label {
    vertical-align: middle;
  }
`

class Field extends React.Component {
  renderInput = () => {
    const { name, label, required, type, options, ...props } = this.props
    let inputProps = {
      id: `${name}_id`,
      ...this.props,
      ...props.input
    }
    
    if (type === 'checkbox') {
      inputProps = {
        ...inputProps,
        checked: inputProps.input.value
      }
    }
    
    const renderInputFirst = type === 'checkbox' || type === 'radio'
    let InputComponent
    
    switch (type) {
      // case 'select':
      //   InputComponent = Select
      //   break
      // case 'tags':
      //   InputComponent = TagsInput
      //   break
      // case 'file':
      //   InputComponent = FileInput
      //   break
      // case 'daterange':
      //   InputComponent = DateInput.Range
      //   break
      // case 'date':
      //   InputComponent = DateInput.Calendar
      //   break
      // case 'wysiwyg':
      //   InputComponent = Wysiwyg
      //   break
      default:
        InputComponent = Input
    }
    
    return (
      <div>
        {renderInputFirst && <InputComponent {...inputProps} />}
        {/*{label && (*/}
          {/*<Label htmlFor={inputProps.id}>*/}
            {/*{label}*/}
            {/*{required && <FieldAsterisk>*</FieldAsterisk>}*/}
          {/*</Label>*/}
        {/*)}*/}
        {renderInputFirst || <InputComponent {...inputProps} />}
      </div>
    )
  }
  render() {
    const {
      // eslint-disable-next-line
      meta,
      children,
      bottomOffset,
      visible,
      flex,
      type,
      minWidth
    } = this.props
    
    const render = (
      <Wrapper
        flex={flex}
        type={type}
        bottomOffset={bottomOffset}
        minWidth={minWidth}
      >
        {children || this.renderInput()}
        {/*{meta.error &&*/}
        {/*meta.touched && (*/}
          {/*<Error id={`${name}Error`} role="alert" palette="danger">*/}
            {/*{meta.error}*/}
          {/*</Error>*/}
        {/*)}*/}
      </Wrapper>
    )
    
    return visible ? render : null
  }
}

Field.propTypes = {
  visible: PropTypes.bool,
  autofocus: PropTypes.bool,
  name: PropTypes.string,
  label: PropTypes.any,
  required: PropTypes.bool,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  input: PropTypes.object,
  meta: PropTypes.object,
  options: PropTypes.array,
  flex: PropTypes.number,
  bottomOffset: PropTypes.number,
  onChange: PropTypes.func,
  value: PropTypes.any,
  minWidth: PropTypes.number,
  height: PropTypes.number,
  disabled: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

Field.defaultProps = {
  visible: true,
  type: 'text',
  options: [],
  input: {},
  meta: {},
  flex: 1,
  focus: false
}

export default Field
