import React from 'react'
import styled from 'styled-components'
import {font} from 'styled-theme'

const Wrapper = styled.p`
  font-family: ${font('primary')};
  font-size: 0.8rem;
  line-height: 1.3;
  margin: 1rem;
  color: rgba(102, 102, 102, 0.7);
`

const Copyright = () => {
  const currentYear = new Date().getFullYear();
  
  return (
    <Wrapper>©{currentYear}</Wrapper>
  )
}

export default Copyright
