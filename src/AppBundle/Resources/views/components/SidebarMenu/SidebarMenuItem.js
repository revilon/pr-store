import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'

const mapStateToProps = (state) => {
  return state
}

const SidebarSubmenuItems = styled.div`
  display: inline-block;
  margin-left: auto;
  margin-right: 15%;
  background-color: #2b96f1;
  padding: 2px 5px 1px;
  border-radius: 2px;
  font-size: 12px;
  min-width: 20px;
  text-align: center;
`
const SidebarMenuItemSubmenu = styled.div`
  padding:0 0 0 50px;
  font-size: 14px;
  height: 40px;
  color: #fff;
  display: flex;
  align-items: center;
  cursor: pointer;
  &:focus, &:active, &:hover {
    color: #000;
    background-color: #6bddff;
  }
`
const SidebarMenuItemTitle = styled.p`
  font-size: 12px;
  display: inline-block;
`

class SidebarMenuItemWrapper extends React.Component {
  constructor(props) {
    super(props)
    this.setCurrentType = this.setCurrentType.bind(this)
  }
  
  static propTypes = {
    name: PropTypes.string,
    children: PropTypes.any,
    type: PropTypes.string,
    taskId: PropTypes.string
  }
  
  setCurrentType() {
    this.props.setCurrentType(this.props.type)
    this.props.setCurrentTask(this.props.taskId)
    this.props.clickCount()
    
    if (this.props.type === 'all') {
      this.props.taskBySocial(this.props.webSocket, this.props.taskId)
    }
    else {
      this.props.taskBySocial(this.props.webSocket, this.props.taskId, this.props.type)
    }
  }
  
  render() {
    const {name, children} = this.props
    
    return (
      <SidebarMenuItemSubmenu onClick={this.setCurrentType}>
        <SidebarMenuItemTitle>
          {name}
        </SidebarMenuItemTitle>
        <SidebarSubmenuItems>
          {children}
        </SidebarSubmenuItems>
      </SidebarMenuItemSubmenu>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(SidebarMenuItemWrapper)
