import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  text-align: center;
`

class Logo extends React.Component {
	render() {
		return (
			<Wrapper>
				LOGO
			</Wrapper>
		)
	}
}

export default Logo
