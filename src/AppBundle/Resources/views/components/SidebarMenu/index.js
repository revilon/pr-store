import React from 'react'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'
import styled from 'styled-components'
import SidebarMenuItem from './SidebarMenuItem'
import Accordion from '../Accordion'
import Icon from '../Fa-Icon'

const mapStateToProps = (state) => {
  return state
}

const SidebarMenuWrapper = styled.div``
const IconWrapper = styled.div`
  display: inline-block;
  padding: 0 5px;
`
const Count = styled.div`
  display: inline-block;
  color: #777;
  background-color: #333;
  padding: 3px 10px;
  border-radius: 12px;
  font-size: 12px;
  text-align: center;
`
const CountWrapper = styled.div`
  display: flex;
  align-items: center;
  font-size: 15px;
`
const GroupTitle = styled.div`
  color: #aaa;
  padding: 25px 20px 5px;
  font-size: 10px;
  font-weight: 600;
  line-height: 1.1;
  text-transform: uppercase;
`
const Title = styled.div`
  margin-right: 10%;
`

class SidebarMenu extends React.Component {
  constructor(props) {
    super(props)
    this.getTasks = this.getTasks.bind(this)
  }
  
  componentWillMount() {
    this.getTasks()
    this.getTimeout()
  }
  
  getTimeout() {
    this.props.webSocket.send(JSON.stringify({'method': 'taskTotal','data': {}}));
    setTimeout( ()=> {
      this.getTimeout()
    }, 60000)
  }
  
  getTasks() {
    this.props.getTaskTotal(this.props.webSocket)
  }
  
  lengthArr = (arr) => {
    return Object.keys(arr).length + 1
  }
  
  renderIcon = (icon) => {
    return (
      <IconWrapper>
        <Icon height={22} name={`${icon} fab`}/>
      </IconWrapper>
    )
  }
  
  renderCountAllTasks = (arr) => {
    let array = Object.values(arr[0])
    let sum = 0;
    for (let i = 0; i < array.length; i++) {
      sum += array[i]
    }
    return sum
  }
  
  renderTitle = (name, arr) => {
    const array = []
    array.push(arr)
    
    return (
      <CountWrapper>
        <Title>
          {name}
        </Title>
        <Count>
          {this.renderCountAllTasks(array)}
        </Count>
      </CountWrapper>
    )
  }

  renderSidebarMenu = (prop, id) => {
    const array = []
    array.push(prop)
    
    return (
      <div>
        {array && array.map(({like, share, subscribe, comment, vote, view}, key) => (
          <SidebarMenuWrapper key={`menu-${key}`}>
            {array && (
              <SidebarMenuItem
                name="Все задания"
                type="all"
                taskId={id}
                webSocket={this.props.webSocket}
              >
                {this.renderCountAllTasks(array)}
              </SidebarMenuItem>
            )}
            {like && (
              <SidebarMenuItem
                name="Мне нравится"
                type="like"
                taskId={id}
                webSocket={this.props.webSocket}
              >
                {like}
              </SidebarMenuItem>
            )}
            {share && (
              <SidebarMenuItem
                name="Рассказать друзьям"
                type="share"
                taskId={id}
                webSocket={this.props.webSocket}
              >
                {share}
              </SidebarMenuItem>
            )}
            {subscribe && (
              <SidebarMenuItem
                name="Подписчики"
                type="subscribe"
                taskId={id}
                webSocket={this.props.webSocket}
              >
                {subscribe}
              </SidebarMenuItem>
            )}
            {comment && (
              <SidebarMenuItem
                name="Комментарии"
                type="comment"
                taskId={id}
                webSocket={this.props.webSocket}
              >
                {comment}
              </SidebarMenuItem>
            )}
            {vote && (
              <SidebarMenuItem
                name="Голосование"
                type="vote"
                taskId={id}
                webSocket={this.props.webSocket}
              >
                {vote}
              </SidebarMenuItem>
            )}
            {view && (
              <SidebarMenuItem
                name="Просмотры"
                type="view"
                taskId={id}
                webSocket={this.props.webSocket}
              >
                {view}
              </SidebarMenuItem>
            )}
          </SidebarMenuWrapper>
        ))}
      </div>
    )
  }
  
  render() {
    const {taskTotal} = this.props
    
    return (
      <div>
        <GroupTitle>
          Новые задания
        </GroupTitle>
        {taskTotal && taskTotal.map((prop, key) => (
          <Accordion
            title={this.renderTitle(prop.name, prop.task_type)}
            icon={this.renderIcon(prop.id)}
            key={`main-menu-${key}-${prop.name}`}
            items={this.lengthArr(prop.task_type)}
            lastItem={key === taskTotal.length - 1}
          >
            {this.renderSidebarMenu(prop.task_type, prop.id)}
          </Accordion>
        ))}
      </div>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(SidebarMenu)
