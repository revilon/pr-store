import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { font, palette } from 'styled-theme'
import { ifProp } from 'styled-tools'


const fontSize = ({ height }) => `${height / 35.5555555556}rem`
const borderColor = ({ meta }) => {
  const Color = meta.error && meta.touched
    ? palette('danger', 2)
    : palette('grayscale', 3);
  return Color;
}
const Height = props => (
  (props.type === 'textarea' ? 'auto' : props.height) + 'px'
)

const styles = css`
  font-family: ${font('primary')};
  display: block;
  width: 100%;
  margin: 0;
  box-sizing: border-box;
  font-size: ${fontSize};
  padding: ${ifProp({ type: 'textarea' }, '0.4444444444em', '0 0.4444444444em')};
  height: ${Height};
  color: ${palette('grayscale', 0)};
  background-color: ${palette('grayscale', 0, true)};
  border: 1px solid ${borderColor};
  border-radius: 2px;

  ::placeholder,
  ::-webkit-input-placeholder {
    color: ${palette('grayscale', 2, true)};
    font-weight: 400;
  }

  &:focus {
    outline-color: #40a3f5;
    outline-style: auto;
    outline-width: 5px;
  }

  &[type=checkbox], &[type=radio] {
    display: inline-block;
    border: 0;
    border-radius: 0;
    width: auto;
    height: auto;
    margin: 0 0.2rem 0 0;
  }
  &[type=file] {
    border-width: 0;
    padding: 0;
    height: auto;
  }
  &:disabled {
    background: ${palette('grayscale', 1, true)};
    cursor: not-allowed;
  }
`

const StyledTextarea = styled.textarea`${styles}`
const StyledRawCodeEditor = styled.textarea`${styles}`
const StyledInput = styled.input`${styles}`

class Input extends React.Component {
  render () {
    switch (this.props.type) {
      case 'textarea':
        return <StyledTextarea meta={this.props.meta} {...this.props} />
      case 'code':
        return <StyledRawCodeEditor meta={this.props.meta} {...this.props} />
      default:
        return <StyledInput meta={this.props.meta} {...this.props} />
    }
  }
}

Input.propTypes = {
  type: PropTypes.string,
  meta: PropTypes.object,
}

Input.defaultProps = {
  type: 'text',
  height: 40,
  meta: {},
}

export default Input
