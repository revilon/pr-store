import React from 'react'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'
import styled from 'styled-components'

import Field from '../Field'
import Button from '../Button'
import FormErrors from './FormErrors'

const mapStateToProps = (state) => {
  return state
}

const FormBody = styled.div`
  height: 160px;
`
const ErrorDescription = styled.div`
  padding: 5px;
  color: red;
`

class Registration extends React.Component {
  constructor(props) {
    super(props)
    this.getLoginValue = this.getLoginValue.bind(this)
    this.getPasswordValue = this.getPasswordValue.bind(this)
    this.getPasswordRepeatedValue = this.getPasswordRepeatedValue.bind(this)
    this.getCorrectPassword = this.getCorrectPassword.bind(this)
    this.sendDataFirst = this.sendDataFirst.bind(this)
    this.sendDataTwice = this.sendDataTwice.bind(this)
    this.getErrorStatus = this.getErrorStatus.bind(this)
    this.getSuccessStatus = this.getSuccessStatus.bind(this)
    this.validateEmailField = this.validateEmailField.bind(this)
    this.validatePasswordField = this.validatePasswordField.bind(this)
    this.validateForm = this.validateForm.bind(this)
    this._handleKeyPress = this._handleKeyPress.bind(this)
  }
  
  state = {
    login: '',
    password: '',
    repeat_password: '',
    correct_password: '',
    error: false,
    errorDescription: 'Придумайте пароль',
    disabled: true,
    formErrors: {email: '', password: ''},
    emailValid: false,
    passwordValid: false,
    password_valid: false,
    repeat_password_valid: false,
    loader: false,
    buttonMessage: 'Зарегистрироваться'
  }
  
  componentWillReceiveProps(nextProps) {
    if (this.props.registration_error_description !== nextProps.registration_error_description) {
      this.getErrorStatus(this.props.registration_status, nextProps.registration_error_description)
    }
    if (this.props.registration_status !== nextProps.registration_status || this.props.registration_error_description !== nextProps.registration_error_description) {
      this.getErrorStatus(nextProps.registration_status, nextProps.registration_error_description)
    }
    this.getSuccessStatus(nextProps.registration_status)
  }
  
  getErrorStatus(status, description) {
    switch (status) {
      case 'error':
        this.setDisableButton()
        this.setState({
          ...this.state,
          errorDescription: description,
          error: true
        }, () => {
          this.removeLoader()
        })
        break
      default:
        return null
    }
  }
  
  getSuccessStatus(status) {
    switch (status) {
      case 'success':
        this.setDisableButton()
        this.setLoader()
        this.setState({
          ...this.state,
          error: false,
          errorDescription: '',
        }, () => {
        })
        window.location = '/'
        break
      default:
        return null
    }
  }
  
  getLoginValue(event) {
    this.setState({
        ...this.state,
        login: event.target.value,
        errorDescription: '',
      },
      () => {
        this.validateEmailField(this.state.login)
      })
  }
  
  getPasswordValue(event) {
    this.setState({
        ...this.state,
        password: event.target.value,
        errorDescription: '',
      },
      () => {
        this.getCorrectPassword()
        this.validatePasswordField()
      })
  }
  
  getPasswordRepeatedValue(event) {
    this.setState({
        ...this.state,
        repeat_password: event.target.value,
        errorDescription: '',
      },
      () => {
        this.getCorrectPassword()
        this.validatePasswordField()
      })
  }
  
  getCorrectPassword() {
    const {password, repeat_password} = this.state
    
    if (password !== '' && repeat_password !== '' && password === repeat_password) {
      this.setState({
        ...this.state,
        correct_password: repeat_password,
        disabled: false,
        errorDescription: ''
      })
    } else {
      this.setState({
        ...this.state,
        error: true,
        errorDescription: 'пароли не совпадают',
        disabled: true
      })
    }
  }
  
  validateEmailField(value) {
    let {emailValid, formErrors} = this.state
    
    emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    formErrors.email = emailValid ? '' : 'Неправильный формат e-mail'
    this.setState({
      formErrors: formErrors,
      emailValid: emailValid,
    }, this.validateForm)
  }
  
  validatePasswordField() {
    let {passwordValid, formErrors, password, repeat_password, password_valid, repeat_password_valid} = this.state
    
    password_valid = password.length >= 6 && password.length <= 20 && !password.includes(' ')
    repeat_password_valid = repeat_password.length >= 6 && repeat_password.length <= 20 && !repeat_password.includes(' ')
    passwordValid = password_valid && repeat_password_valid
    formErrors.password = passwordValid ? '' : 'Пароль должен быть длиной от 6 до 20 любых символов, без пробелов.'
    if (password !== '' && repeat_password !== '' && password === repeat_password) {
      this.setState({
        formErrors: formErrors,
        passwordValid: passwordValid,
      }, this.validateForm)
    }
  }
  
  validateForm() {
    this.setState({
      disabled: !this.state.emailValid || !this.state.passwordValid
    })
  }
  
  setDisableButton() {
    this.setState({
      ...this.state,
      disabled: true,
      buttonMessage: 'Идет проверка...'
    })
  }
  
  removeLoader() {
    this.setState({
      ...this.state,
      loader: false
    }, () => {
      this.props.setLoader(this.state.loader)
      this.setEnableButton()
    })
  }
  
  setEnableButton() {
    this.setState({
      ...this.state,
      disabled: false,
      buttonMessage: 'Зарегистрироваться'
    })
  }
  
  setLoader() {
    this.setState({
      ...this.state,
      loader: true,
    }, () => {
      this.props.setLoader(this.state.loader)
      this.setDisableButton()
    })
  }
  
  sendDataFirst(login, correct_password, registration_status, registration_error_description) {
    this.setDisableButton()
    this.setLoader()
    this.props.clearRegistrationErrorDescription()
    this.props.sendDataRegistration(this.props.webSocket, {email: login, password: correct_password})
    this.getErrorStatus(registration_status, registration_error_description)
    this.getSuccessStatus(registration_status)
  }
  
  sendDataTwice(registration_status, registration_error_description) {
    this.setLoader()
    this.props.clearRegistrationErrorDescription()
    this.getSuccessStatus(registration_status)
    this.getErrorStatus(registration_status, registration_error_description)
  }
  
  _handleKeyPress = (e, login, correct_password, registration_status, registration_error_description) => {
    if (e.key === 'Enter') {
      if (!this.state.disabled) {
        this.sendDataFirst(this.props.webSocket, login, correct_password, registration_status, registration_error_description)
      }
    }
  }
  
  render() {
    const {
      login,
      password,
      repeat_password,
      correct_password,
      disabled,
      formErrors,
      buttonMessage,
      error,
      errorDescription
    } = this.state
    
    const {registration_status, registration_error_description} = this.props
    
    return (
      <div>
        <FormBody>
          <Field
            type="input"
            name="login"
            value={login}
            placeholder="E-mail"
            onChange={this.getLoginValue}
            onKeyPress={(e) => {
              this._handleKeyPress(e, login, correct_password, registration_status, registration_error_description)
            }}
            height={30}
          />
          <Field
            type="password"
            name="password"
            placeholder="Пароль"
            value={password}
            onChange={this.getPasswordValue}
            onKeyPress={(e) => {
              this._handleKeyPress(e, login, correct_password, registration_status, registration_error_description)
            }}
            height={30}
          />
          <Field
            type="password"
            name="repeat_password"
            placeholder="Повторите пароль"
            value={repeat_password}
            onChange={this.getPasswordRepeatedValue}
            onKeyPress={(e) => {
              this._handleKeyPress(e, login, correct_password, registration_status, registration_error_description)
            }}
            height={30}
          />
        </FormBody>
        <div>
          <Button
            height={35}
            fluid
            palette="success"
            disabled={disabled}
            transparent={disabled}
            onClick={() => {
              this.sendDataFirst(login, correct_password, registration_status, registration_error_description);
              this.sendDataTwice(registration_status, registration_error_description)
            }}
          >
            {buttonMessage}
          </Button>
        </div>
        {error && (
          <ErrorDescription>
            {errorDescription}
          </ErrorDescription>
        )}
        {formErrors && (
          <FormErrors formErrors={formErrors}/>
        )}
      </div>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(Registration)
