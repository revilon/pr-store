import React from 'react'
import styled from 'styled-components'

const Error = styled.div`
  padding: 5px;
  color: red;
`

const FormErrors = ({formErrors}) =>
  <div>
    {Object.keys(formErrors).map((fieldName, i) => {
      if(formErrors[fieldName].length > 0){
        return (
          <Error key={i}>{formErrors[fieldName]}</Error>
        )
      } else {
        return ''
      }
    })}
  </div>

export default FormErrors