import React from 'react'
import PropTypes from 'prop-types'
import Field from '../Field'
import Button from '../Button'
import {connect} from 'react-redux'
import {sendForgetPassword} from '../../actions'
import io from 'socket.io-client'
import styled from 'styled-components'
let socket

const BackButtonWrapper = styled.div`
  display: flex;
  padding: 20px 5px;
`

const mapStateToProps = (state = {}) => {
  return {...state}
}

class ForgetPassword extends React.Component {
  constructor(props) {
    super(props)
    socket = io('https://pr-store.9ek.ru:49292', {
      secure: true,
      reconnect: true,
      rejectUnauthorized : false,
      transports: ['websocket'],
      query: {
        auth: localStorage.getItem('pr_store_token')
      }
    })
    
    this.sendForgetPassword = this.sendForgetPassword.bind(this)
    this.onChangeForm = this.onChangeForm.bind(this)
  }
  
  static propTypes = {
    onChangeForm: PropTypes.func
  }
  
  state = {
    email: ''
  }
  
  onChangeForm(){
    this.props.onChangeForm()
  }
  
  sendForgetPassword(dispatch, email) {
    return () => dispatch(sendForgetPassword(socket, email))
  }
  
  handleChange(event) {
    this.setState({
      ...this.state,
      email: event.target.value
    })
  }
  
  render() {
    const {email} = this.state
    const {dispatch, answer} = this.props
    
    return (
      <div>
        <p>Если вы забыли пароль от входа, введите свой почтовый ящик, указанный при регистрации</p>
        <Field height={30} type="text" name="forget_password" placeholder="E-mail" value={email} onChange={this.handleChange}/>
        <Button height={35} fluid palette="primary" onClick={this.sendForgetPassword(dispatch, email)}>Восстановить пароль</Button>
        <BackButtonWrapper>
          <Button height={25}  palette="default" onClick={this.onChangeForm}>Назад к форме входа</Button>
        </BackButtonWrapper>
        <div>{answer}</div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(ForgetPassword)
