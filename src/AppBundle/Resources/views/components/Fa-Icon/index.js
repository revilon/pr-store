import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { palette } from 'styled-theme'
import { ifProp } from 'styled-tools'

const fontSize = ({ width, height }) => {
  const size = width || height
  return size ? `${size / 16}rem` : 'inherit'
}

const Wrapper = styled.i`
  display: inline-block;
  font-size: ${fontSize};
  color: ${ifProp('palette', palette({ grayscale: 0 }, 1), 'currentcolor')};
  box-sizing: border-box;
`

const Icon = ({ name, type, ...props }) => !!name && (
  <Wrapper {...props} className={`${type}-${name} ${props.className || ''}`} />
)

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  palette: PropTypes.string,
  reverse: PropTypes.bool,
}

Icon.defaultProps = {
  type: 'fa'
}

export default Icon
