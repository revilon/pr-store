import React from 'react'
import styled from 'styled-components'

import Copyright from '../Copyright/index'

const FooterWrapper = styled.div`
  display: flex;
  background-color: hsla(0, 0%, 0%, 0.09);
  padding: 0.5rem;
`
const FooterCopyright = styled.div`
  flex: 0 0 auto;
`

const Footer = () => {
  return (
    <FooterWrapper>
      <FooterCopyright>
        <Copyright />
      </FooterCopyright>
    </FooterWrapper>
  )
}

export default Footer
