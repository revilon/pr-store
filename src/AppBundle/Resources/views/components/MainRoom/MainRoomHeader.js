import React from 'react'
import styled from 'styled-components'

import Icon from '../Fa-Icon'

const IconWrapper = styled.div`
  display: inline-block;
  padding: 0 5px;
`
const Title = styled.div`
  font-family: open sans,Helvetica,sans-serif;
  display: inline-block;
  font-weight: 300;
  color: #555;
  margin: 10px 0;
  border-bottom: 0;
  text-align: center;
  text-transform: capitalize;
`
const SubTitle = styled(Title)`
  font-weight: 600;
  text-transform: none;
`
const Wrapper = styled.div`
  text-align: center;
  font-size: 30px;
  @media (max-width: 767px) {
   display: none;
  }
`

class MainRoomHeader extends React.Component {
  render() {
    const {currentTask, currentType} = this.props
    
    let subTitle
    let title
    switch (currentTask) {
      case 'vk':
        title = 'Вконтакте'
        break
      default:
        title = 'Инстаграм'
    }
    switch (currentType) {
      case 'like':
        subTitle = 'Мне нравится'
        break
      case 'share':
        subTitle = 'Рассказать друзьям'
        break
      case 'subscribe':
        subTitle = 'Подписчики'
        break
      case 'comment':
        subTitle = 'Комментарии'
        break
      case 'vote':
        subTitle = 'Голосование'
        break
      case 'view':
        subTitle = 'Просмотры'
        break
      default:
        subTitle = 'Все задания'
    }
    
    return (
      <Wrapper>
        <IconWrapper>
          <Icon height={30} name={`${currentTask} fab`}/>
        </IconWrapper>
        <Title>{title} - <SubTitle>{subTitle}</SubTitle></Title>
      </Wrapper>
    )
  }
}

export default MainRoomHeader
