import React from 'react'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'
import styled from 'styled-components'

import Field from '../Field'
import Button from '../Button'

const mapStateToProps = (state) => {
  return state
}

const FormBody = styled.form`
  height: 160px;
`
const ErrorDescription = styled.div`
  padding: 5px;
  color: red;
`

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.getLoginValue = this.getLoginValue.bind(this)
    this.getPasswordValue = this.getPasswordValue.bind(this)
    this.setLoader = this.setLoader.bind(this)
    this.removeLoader = this.removeLoader.bind(this)
    this.setDisableButton = this.setDisableButton.bind(this)
    this.setEnableButton = this.setEnableButton.bind(this)
    this.getSuccessStatus = this.getSuccessStatus.bind(this)
    this.getErrorStatus = this.getErrorStatus.bind(this)
    this._handleKeyPressLogin = this._handleKeyPressLogin.bind(this)
    this._handleKeyPressPassword = this._handleKeyPressPassword.bind(this)
    
    this.props.webSocket.onerror = (evt) => {
      console.log('error:', evt)
      this.props.setLoader(this.state.loader, true)
    }
  }
  
  state = {
    login: '',
    password: '',
    errorDescription: '',
    disabled: false,
    loader: false,
    error: false,
    buttonMessage: 'Войти'
  }
  
  componentWillReceiveProps(nextProps) {
    if (this.props.login_status !== nextProps.login_status) {
      this.getSuccessStatus(nextProps.login_status)
    }
    if (this.props.login_error_description !== nextProps.login_error_description || this.props.login_status !== nextProps.login_status) {
      this.getErrorStatus(nextProps.login_status, nextProps.login_error_description)
    }
  }
  
  getSuccessStatus(status) {
    switch (status) {
      case 'success':
        this.setDisableButton()
        this.setLoader()
        this.setState({
          error: false,
          errorDescription: false
        })
        window.location = '/'
        break
      default:
        return null
    }
  }
  
  getErrorStatus(status, login_error_description) {
    switch (status) {
      case 'error':
        this.setDisableButton()
        this.setLoader()
        this.setState({
          error: true,
          errorDescription: login_error_description
        }, () => {
          this.removeLoader()
          this.setEnableButton()
        })
        break
      default:
        return null
    }
  }
  
  getLoginValue(event) {
    this.setState({
      ...this.state,
      login: event.target.value,
      errorDescription: ''
    })
  }
  
  getPasswordValue(event) {
    this.setState({
      ...this.state,
      password: event.target.value,
      errorDescription: ''
    })
  }
  
  setDisableButton() {
    this.setState({
      ...this.state,
      disabled: true,
      buttonMessage: 'Идет проверка...'
    })
  }
  
  setEnableButton() {
    this.setState({
      ...this.state,
      disabled: false,
      buttonMessage: 'Войти'
    })
  }
  
  setLoader() {
    this.setState({
      ...this.state,
      loader: true,
    }, () => {
      this.props.setLoader(true)
    })
  }
  
  removeLoader() {
    this.setState({
      ...this.state,
      loader: false
    }, () => {
      this.props.setLoader(false)
    })
  }
  
  _handleKeyPressLogin = (e, webSocket, login, password, login_status, login_error_description) => {
    if (e.key === 'Enter') {
      this.sendDataFirst(webSocket, login, password, login_status, login_error_description)
    }
  }
  
  _handleKeyPressPassword = (e, webSocket, login, password, login_status, login_error_description) => {
    if (e.key === 'Enter') {
      this.sendDataFirst(webSocket, login, password, login_status, login_error_description)
    }
  }
  
  sendDataFirst(webSocket, login, password, login_status, login_error_description) {
    if (login !== '' && password !== '') {
      this.setLoader()
      this.setDisableButton()
      this.props.clearLoginErrorDescription()
      this.props.sendDataLogin(webSocket, {email: login, password: password})
      this.getSuccessStatus(login_status)
      this.getErrorStatus(login_status, login_error_description)
    } else {
      this.setState({
        error: true,
        errorDescription: "Заполните обязательные поля"
      })
    }
  }
  
  sendDataTwice(login_status, login_error_description) {
    if (this.state.login !== '' && this.state.password !== '') {
      this.setLoader()
      this.setDisableButton()
      this.props.clearLoginErrorDescription()
      this.getSuccessStatus(login_status)
      this.getErrorStatus(login_status, login_error_description)
    } else {
      this.setState({
        error: true,
        errorDescription: "Заполните обязательные поля"
      })
    }
  }
  
  render() {
    const {
      login,
      password,
      errorDescription,
      disabled,
      buttonMessage,
      error
    } = this.state
    
    return (
      <div>
        <FormBody>
          <Field
            type="input"
            name="login"
            value={login}
            onChange={this.getLoginValue}
            height={30}
            onKeyPress={(e) => {
              this._handleKeyPressLogin(e, this.props.webSocket, login, password, this.props.login_status, this.props.login_error_description)
            }}
          />
          <Field
            type="password"
            name="password"
            value={password}
            onChange={this.getPasswordValue}
            height={30}
            onKeyPress={(e) => {
              this._handleKeyPressPassword(e, this.props.webSocket, login, password, this.props.login_status, this.props.login_error_description)
            }}
          />
        </FormBody>
        <div>
          <Button
            height={35}
            fluid
            disabled={disabled}
            transparent={disabled}
            onClick={() => {
              this.sendDataFirst(this.props.webSocket, login, password, this.props.login_status, this.props.login_error_description);
              this.sendDataTwice(this.props.login_status, this.props.login_error_description)
            }}
          >
            {buttonMessage}
          </Button>
          {error && (
            <ErrorDescription>
              {errorDescription}
            </ErrorDescription>
          )}
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(Login)
