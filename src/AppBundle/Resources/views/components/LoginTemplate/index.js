import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {font} from 'styled-theme'
import Footer from '../Footer'

const TemplateWrapper = styled.div`
  font-size: 0.9em;
  position: relative;
  display: flex;
  flex: 1;
  color: rgba(0, 0, 0, 0.87);
  font-family: ${font('primary')};
`
const TemplateContentWrapper = styled.div`
  height: 100%;
  background-color: #ecfcff;
`
const TemplateInnerContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: calc(100% - 64px);
`
const TemplateFooterWrapper = styled.div`
  height: auto;
`
const TemplateMainContentWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100vh;
`

class Template extends React.Component {
  static propTypes = {
    children: PropTypes.any,
  }
  
  render() {
    const {children} = this.props
    
    return (
      <TemplateWrapper>
        <TemplateMainContentWrapper>
          <TemplateContentWrapper>
            <TemplateInnerContent>
              {children}
            </TemplateInnerContent>
            <TemplateFooterWrapper>
              <Footer id="footer"/>
            </TemplateFooterWrapper>
          </TemplateContentWrapper>
        </TemplateMainContentWrapper>
      </TemplateWrapper>
    )
  }
}

export default Template
