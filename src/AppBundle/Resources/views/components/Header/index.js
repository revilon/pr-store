import React from 'react'
import styled from 'styled-components'
import UserInfo from './UserInfo'
import Item from './Item'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  height: 61px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  @media (max-width: 767px) {
   display: none;
  }
`
const HeaderItemWrapper = styled.div`
  margin: 0 10px;
`
const UserInfoWrapper = styled.div`
  text-align: center;
  margin-left: auto;
`

class Header extends React.Component {
  render() {
    return (
      <Wrapper>
	      <HeaderItemWrapper>
		      <Item />
        </HeaderItemWrapper>
	      <HeaderItemWrapper>
		      <Item />
	      </HeaderItemWrapper>
	      <HeaderItemWrapper>
		      <Item />
	      </HeaderItemWrapper>
	      <HeaderItemWrapper>
		      <Item />
	      </HeaderItemWrapper>
	      <HeaderItemWrapper>
		      <Item />
	      </HeaderItemWrapper>
	      <HeaderItemWrapper>
		      <Item />
	      </HeaderItemWrapper>
        <UserInfoWrapper>
	        <UserInfo />
        </UserInfoWrapper>
      </Wrapper>
    )
  }
}

export default Header
