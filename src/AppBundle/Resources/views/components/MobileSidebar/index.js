import React from 'react'
import styled, {css} from 'styled-components'
import UserInfo from '../Header/UserInfo'
import Accordion from '../Accordion'
import Icon from '../Fa-Icon'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'

const mapStateToProps = (state) => {
  return state
}

const collapsedWrapper = ({collapsed}) => collapsed && css`
  left: -260px;
  transition: left 0.25s ease-in-out;
`
const collapsedWrap = ({collapsed}) => collapsed && css`
  display: block;
`
const Wrapper = styled.div`
  display: flex;
  position: absolute;
  flex-direction: column;
  height: 100vh;
  width: 260px;
  background-color: #000;
  color: #fff;
  left: 0;
  overflow-y: auto;
  overflow-x: hidden;
  transition: left 0.25s ease-in-out;
  ${collapsedWrapper};
  @media (min-width: 768px) {
    display: none;
  }
`
const Wrap = styled.div`
  display: none;
  height: 100vh;
  width: 520px;
  left: 260px;
  background-color: #fff;
  opacity: 0.5;
  position: fixed;
  ${collapsedWrap};
`
const MenuItem = styled.div`
  padding: 11px;
  color: #fff;
  &:focus, &:active, &:hover {
    color: #000;
    background-color: #9e9e9e;
  }
`
const IconWrapper = styled.div`
  display: inline-block;
  padding: 0 5px;
`
const Title = styled.div`
  display: inline-block;
  text-transform: capitalize;
  font-size: 14px;
`

class MobileSidebar extends React.Component {
  constructor(props) {
    super(props)
    this.closeSidebar = this.closeSidebar.bind(this)
    this.renderCheat = this.renderCheat.bind(this)
    this.onClickRenderTasks = this.onClickRenderTasks.bind(this)
    this.renderTasks = this.renderTasks.bind(this)
  }
  
  closeSidebar() {
    this.props.onCollapseSidebar(true)
  }
  
  renderTasks(tasks) {
    this.props.renderMobileHeaderItems(tasks)
  }
  
  onClickRenderTasks = (tasks) => {
    return () => {
      this.renderTasks(tasks)
      this.props.setCurrentTask(tasks)
      this.props.setCurrentType('all')
      this.props.clickCount()
      this.props.taskBySocial(this.props.webSocket, tasks)
    }
  }
  
  renderCheat(taskTotal) {
    return taskTotal && taskTotal.map((prop, key) => (
      <MenuItem key={`main-menu-${key}`} onClick={this.onClickRenderTasks(prop.id)}>
        <IconWrapper>
          <Icon name={`${prop.id} fab`}/>
        </IconWrapper>
        <Title>
          {prop.name}
        </Title>
      </MenuItem>
    ))
  }
  
  render() {
    const {collapsed, taskTotal} = this.props
    
    return (
      <div>
        <Wrapper collapsed={collapsed}>
          <Accordion
            title={<UserInfo/>}
            key={`main-menu-UserInfo`}
            items={3}
          >
            <div>Мои данные</div>
            <div>Мои настройки</div>
            <div>Мои уведомления</div>
          </Accordion>
          {taskTotal && (
            <Accordion
              title={<div>Заработать</div>}
              key={`main-menu-tzarabotat`}
              items={taskTotal.length}
            >
              {taskTotal && this.renderCheat(taskTotal)}
            </Accordion>
          )}
          <Accordion
            title={<div>Накрутить</div>}
            key={`main-menu-nakruti`}
            items={3}
          >
            <MenuItem onClick={this.closeSidebar}>
              <IconWrapper>
              </IconWrapper>
              <Title>
                <div>Мои данные</div>
              </Title>
            </MenuItem>
            <MenuItem onClick={this.closeSidebar}>
              <IconWrapper>
              </IconWrapper>
              <Title>
                <div>Мои настройки</div>
              </Title>
            </MenuItem>
            <MenuItem onClick={this.closeSidebar}>
              <IconWrapper>
              </IconWrapper>
              <Title>
                <div>Мои уведомления</div>
              </Title>
            </MenuItem>
          </Accordion>
          <Accordion
            title={<div>Конкурсы</div>}
            key={`main-menu-konkursi`}
            items={3}
          >
            <div>Мои данные</div>
            <div>Мои настройки</div>
            <div>Мои уведомления</div>
          </Accordion>
          <Accordion
            title={<div>Инфо</div>}
            key={`main-menu-info`}
            items={3}
          >
            <div>Мои данные</div>
            <div>Мои настройки</div>
            <div>Мои уведомления</div>
          </Accordion>
          <Wrap collapsed={!collapsed} onClick={this.closeSidebar}/>
        </Wrapper>
      </div>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(MobileSidebar)
