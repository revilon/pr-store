import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'
import styled from 'styled-components'
import {font} from 'styled-theme'

import Header from '../../components/Header'
import MobileHeader from '../../components/MobileHeader'
import Sidebar from '../../containers/Sidebar'
import MobileSidebar from '../../components/MobileSidebar'
import Loader from '../../components/Loader'

const mapStateToProps = (state) => {
  return state
}

const LoaderWrapper = styled.div`
  position: fixed;
  z-index: 9999;
  height: 100%;
  width: 100%;
  background-color: #ffffff4d;
`
const TemplateWrapper = styled.div`
  min-height: 100vh;
  font-size: 0.9em;
  position: relative;
  flex: 1;
  overflow: hidden;
  display: flex;
  color: rgba(0, 0, 0, 0.87);
  font-family: ${font('primary')};
`
const TemplateContentWrapper = styled.div`
  overflow-y: auto;
  height: calc(100% - 59px);
  background-color: #f6f5f3;
`
const TemplateInnerContent = styled.div`
  min-height: calc(100% - 61px);
`
const TemplateSidebarWrapper = styled.div`
  z-index: 99;
`
const TemplateHeaderWrapper = styled.div`
  height: 62px;
  @media (max-width: 767px) {
    height: 59px;
  }
`
const TemplateMainContentWrapper = styled.div`
  position: relative;
  height: 100vh;
  width: 100%;
`

class Template extends React.Component {
  constructor(props) {
    super(props)
    this.onCollapse = this.onCollapse.bind(this)
    this.renderMobileHeaderItems = this.renderMobileHeaderItems.bind(this)
    this.setLoader = this.setLoader.bind(this)
    
    this.props.webSocket.onerror = function (evt, e) {
      console.log('Error occured: ' + evt.data);
    }
  }
  
  static propTypes = {
    children: PropTypes.any,
  }
  
  state = {
    collapsed: true,
    items: '',
    loader: true,
    error_loader: false
  }
  
  componentWillReceiveProps(nextProps) {
    if (this.props.taskTotal !== nextProps.taskTotal) {
      this.removeLoader()
    }
  }
  
  onCollapse(collapsed) {
    this.setState({
      ...this.state,
      collapsed: collapsed
    })
  }
  
  setLoader(loader, error) {
    this.setState({
      ...this.state,
      loader: loader,
      error_loader: error
    })
  }
  
  removeLoader() {
    this.setState({
      ...this.state,
      loader: false,
      error_loader: false
    })
  }
  
  renderMobileHeaderItems(items) {
    this.setState({
      ...this.state,
      items: items
    }, () => {
      this.onCollapse(true)
    })
  }
  
  render() {
    const {children, loaderGlobal} = this.props
    const {collapsed, items, loader, error_loader} = this.state
    
    return (
      <TemplateWrapper>
        {loader && (
          <LoaderWrapper>
            <Loader error_loader={error_loader}/>
          </LoaderWrapper>
        )}
        <TemplateSidebarWrapper>
          <Sidebar webSocket={this.props.webSocket}/>
          <MobileSidebar
            collapsed={collapsed}
            onCollapseSidebar={this.onCollapse}
            renderMobileHeaderItems={this.renderMobileHeaderItems}
            webSocket={this.props.webSocket}
          />
        </TemplateSidebarWrapper>
        <TemplateMainContentWrapper>
          <TemplateHeaderWrapper>
            <Header/>
            <MobileHeader
              webSocket={this.props.webSocket}
              onCollapseHeader={this.onCollapse}
              items={items}
            />
          </TemplateHeaderWrapper>
          <TemplateContentWrapper>
            <TemplateInnerContent>
              {children}
            </TemplateInnerContent>
          </TemplateContentWrapper>
        </TemplateMainContentWrapper>
      </TemplateWrapper>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(Template)
