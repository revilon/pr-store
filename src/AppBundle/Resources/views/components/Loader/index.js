import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import './loader.css'

const Wrapper = styled.div`
  position: fixed;
  z-index: 9999;
  height: 100%;
  width: 100%;
  background-color: #ffffff4d;
`
const ErrorLoader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 60%;
  color: red;
`

class Loader extends React.Component {
  static propTypes = {
    error_loader: PropTypes.bool
  }
  render() {
    const {error_loader} = this.props
    
    return (
      <Wrapper>
        <div className="cssload-container">
          <div className="cssload-whirlpool"></div>
          {error_loader && (
            <ErrorLoader>Проблемы с сетью. Обновите страницу</ErrorLoader>
          )}
        </div>
      </Wrapper>
    )
  }
}

export default Loader
