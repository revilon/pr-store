import React from 'react'
import styled from 'styled-components'
import Accordion from '../Accordion'
import Icon from '../Fa-Icon'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'
import MobileHeaderMenuItem from './MobileHeaderMenuItem'
import MobileHeaderMenu from './MobileHeaderMenu'

const mapStateToProps = (state) => {
  return state
}

const Wrapper = styled.div`
  display: flex;
  align-items: stretch;
  height: 58px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  @media (min-width: 768px) {
    display: none;
  }
`
const IconWrapper = styled.div`
  flex: 0 0 auto;
  padding: 0 20px;
  margin: auto;
`
const MobileHeadeMenuWrapper = styled.div`
  border-bottom: 1px solid #dadada;
`

class MobileHeader extends React.Component {
  constructor(props) {
    super(props)
    this.openSidebar = this.openSidebar.bind(this)
    this.renderItems = this.renderItems.bind(this)
    this.renderMobileHeader = this.renderMobileHeader.bind(this)
    this.changeMobileHeader = this.changeMobileHeader.bind(this)
  }
  
  state = {
    name: 'Все задания',
    icon: 'check fas',
    selected: 'Все задания'
  }
  
  openSidebar() {
    this.props.onCollapseHeader(false)
  }
  
  changeMobileHeader(name, icon, selected) {
    this.setState({
      name: name,
      icon: icon,
      selected: selected
    })
  }
  
  renderMobileHeader = (name, icon, currentType) => {
    if (currentType === 'all') {
      return <MobileHeaderMenu icon="check fas" name="Все задания"/>
    } else {
      return <MobileHeaderMenu icon={icon} name={name}/>
    }
  }
  
  renderItems(items, id, key, tasks_type, currentTask, selected, currentType) {
    const array = []
    let item = items
    let selectedItem = selected
    array.push(tasks_type)
    
    if (currentType === 'all') {
      selectedItem = 'Все задания'
    }
    
    if (!item) {
      item = currentTask
    }
    
    switch (id) {
      case item:
        return (
          <div key={`item-${key}`}>
            {array && array.map(({like, share, subscribe, comment, vote, view}, key) => (
              <MobileHeadeMenuWrapper key={`menu-${key}`}>
                {array && (
                  <MobileHeaderMenuItem
                    name="Все задания"
                    icon="check fas"
                    selectedItem={selectedItem}
                    type="all"
                    changeMobileHeader={this.changeMobileHeader}
                    webSocket={this.props.webSocket}
                  />
                )}
                {like && (
                  <MobileHeaderMenuItem
                    name="Мне нравится"
                    icon="heart far"
                    selectedItem={selectedItem}
                    type="like"
                    changeMobileHeader={this.changeMobileHeader}
                    webSocket={this.props.webSocket}
                  />
                )}
                {share && (
                  <MobileHeaderMenuItem
                    name="Рассказать друзьям"
                    icon="share fas"
                    selectedItem={selectedItem}
                    type="share"
                    changeMobileHeader={this.changeMobileHeader}
                    webSocket={this.props.webSocket}
                  />
                )}
                {subscribe && (
                  <MobileHeaderMenuItem
                    name="Подписчики"
                    icon="user far"
                    selectedItem={selectedItem}
                    type="subscribe"
                    changeMobileHeader={this.changeMobileHeader}
                    webSocket={this.props.webSocket}
                  />
                )}
                {comment && (
                  <MobileHeaderMenuItem
                    name="Комментарии"
                    icon="comment far"
                    selectedItem={selectedItem}
                    type="comment"
                    changeMobileHeader={this.changeMobileHeader}
                    webSocket={this.props.webSocket}
                  />
                )}
                {vote && (
                  <MobileHeaderMenuItem
                    name="Голосование"
                    icon="chart-bar far"
                    selectedItem={selectedItem}
                    type="vote"
                    changeMobileHeader={this.changeMobileHeader}
                    webSocket={this.props.webSocket}
                  />
                )}
                {view && (
                  <MobileHeaderMenuItem
                    name="Просмотры"
                    icon="eye far"
                    selectedItem={selectedItem}
                    type="view"
                    changeMobileHeader={this.changeMobileHeader}
                    webSocket={this.props.webSocket}
                  />
                )}
              </MobileHeadeMenuWrapper>
            ))}
          </div>
        )
      default:
        return null
    }
  }
  
  lengthArr = (taskTotal, currentTask) => {
    if(taskTotal) {
      for (let task of taskTotal) {
        if (task.id === currentTask) {
          return Object.keys(task.task_type).length + 1
        }
      }
    }
  }
  
  render() {
    const {taskTotal, items, currentTask, currentType} = this.props
    const {name, icon, selected} = this.state
    
    return (
      <Wrapper>
        <IconWrapper>
          <Icon name="bars fas" height={30} onClick={this.openSidebar}/>
        </IconWrapper>
        <Accordion
          fluid
          title={this.renderMobileHeader(name, icon, currentType)}
          key={`main-menu`}
          mobileHeader
          items={this.lengthArr(taskTotal, currentTask)}
        >
          {taskTotal && taskTotal.map((prop, key) => (
            this.renderItems(items, prop.id, key, prop.task_type, currentTask, selected, currentType)
          ))}
        </Accordion>
      </Wrapper>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(MobileHeader)
