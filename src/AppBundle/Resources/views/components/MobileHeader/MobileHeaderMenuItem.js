import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {ifProp} from 'styled-tools'
import Icon from '../Fa-Icon'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'

const mapStateToProps = (state) => {
  return state
}

const HeaderSubmenuIcon = styled.div`
  display: inline-block;
  padding: 0 23px;
`
const HeaderMenuItemSubmenu = styled.div`
  font-size: 14px;
  height: 40px;
  color: ${ifProp('selectedName', '#2b96f1', '#000')};
  background-color: ${ifProp('selectedName', '#fafbfc', '#fff')};
  display: flex;
  align-items: center;
  cursor: pointer;
  &:focus, &:active, &:hover {
    outline: none;
    color: #000;
    background-color: #dadada;
  }
`
const HeaderMenuItemTitle = styled.p`
  font-size: 14px;
  padding: 0 10px;
  display: inline-block;
`

class MobileHeaderMenuItem extends React.Component {
  constructor(props) {
    super(props)
    this.changeMobileHeader = this.changeMobileHeader.bind(this)
  }
  
  static propTypes = {
    name: PropTypes.any,
    icon: PropTypes.string,
    type: PropTypes.string,
    selectedItem: PropTypes.string
  }
  
  state = {
    name: '',
    icon: '',
  }
  
  changeMobileHeader() {
    this.setState({
      name: this.props.name,
      icon: this.props.icon,
    }, () => {
      this.props.changeMobileHeader(this.state.name, this.state.icon, this.props.name)
      this.props.setCurrentType(this.props.type)
      this.props.clickCount()
      this.props.taskBySocial(this.props.webSocket, this.props.currentTask, this.props.type)
    })
  }
  
  render() {
    const {name, icon, selectedItem} = this.props
    let selectedName = ''
    
    if (selectedItem === name) {
      selectedName = selectedItem
    }
    
    return (
      <HeaderMenuItemSubmenu selectedName={selectedName} onClick={this.changeMobileHeader}>
        <HeaderSubmenuIcon>
          <Icon name={icon} height={20}/>
        </HeaderSubmenuIcon>
        <HeaderMenuItemTitle>
          {name}
        </HeaderMenuItemTitle>
      </HeaderMenuItemSubmenu>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(MobileHeaderMenuItem)
