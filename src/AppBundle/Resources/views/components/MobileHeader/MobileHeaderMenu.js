import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Icon from '../Fa-Icon'

const IconWrapper = styled.div`
  display: inline-block;
  padding: 0 5px;
`
const MobileHeaderMenuWrapper = styled.div`
  display: flex;
  align-items: center;
`
const HeaderMenuItemTitle = styled.p`
  font-size: 14px;
  display: inline-block;
`

const MobileHeaderMenu = ({name, icon}) => (
  <MobileHeaderMenuWrapper>
    <IconWrapper>
      <Icon name={icon} height={20}/>
    </IconWrapper>
    <HeaderMenuItemTitle>
      {name}
    </HeaderMenuItemTitle>
  </MobileHeaderMenuWrapper>
)

MobileHeaderMenu.propTypes = {
  name: PropTypes.any,
  icon: PropTypes.string
}

export default MobileHeaderMenu
