export const sendDataLogin = (webSocket, item) => {
  return (dispatch) => {
    const data = {
      'method': 'login',
      'data': {
        email: item.email,
        password: item.password,
      }
    }
    webSocket.send(JSON.stringify(data))
    webSocket.onmessage = (evt) => {
      let parse = JSON.parse(evt.data)
      switch (parse['method']) {
        case 'login':
          localStorage.setItem('pr_store_token', parse.data.token)
          dispatch(getLoginToken(parse.data.token))
          dispatch(getLoginStatus(parse.status))
          break
        case 'handler.error':
          if (parse.status === 'error') {
            dispatch(getLoginStatus(parse.status))
            dispatch(getLoginErrorDescription(parse.data.description))
          }
          break
        default:
          return null
      }
    }
  }
}

export function getLoginToken(token) {
  return {
    type: "GET_LOGIN_TOKEN",
    login_token: token,
  }
}

export function getLoginStatus(status) {
  return {
    type: "GET_LOGIN_STATUS",
    login_status: status
  }
}

export function getLoginErrorDescription(description) {
  return {
    type: "GET_LOGIN_ERROR_DESCRIPTION",
    login_error_description: description
  }
}

export const clearLoginErrorDescription = () => {
  return (dispatch) => {
    dispatch(clearLoginErrorDescriptionFn())
  }
}

export function clearLoginErrorDescriptionFn() {
  return {
    type: "GET_LOGIN_ERROR_DESCRIPTION",
    login_error_description: false
  }
}

// --------------------------------------------------------------------------------------------
// отправка логина/пароля для регистрации
export const sendDataRegistration = (webSocket, item) => {
  return (dispatch) => {
    const data = {
      'method': 'registration',
      'data': {
        email: item.email,
        password: item.password,
      }
    }
    webSocket.send(JSON.stringify(data))
    webSocket.onmessage = (evt) => {
      let parse = JSON.parse(evt.data)
      switch (parse['method']) {
        case 'registration':
          localStorage.setItem('pr_store_token', parse.data.token)
          dispatch(getRegistrationToken(parse.data.token))
          dispatch(getRegistrationStatus(parse.status))
          break
        case 'handler.error':
          if (parse.status === 'error') {
            dispatch(getRegistrationStatus(parse.status))
            dispatch(getRegistrationErrorDescription(parse.data.description))
          }
          break
        default:
          return null
      }
    }
  }
}

export function getRegistrationToken(token) {
  return {
    type: "GET_REGISTRATION_TOKEN",
    registration_token: token,
  }
}

export function getRegistrationStatus(status) {
  return {
    type: "GET_REGISTRATION_STATUS",
    registration_status: status
  }
}

export function getRegistrationErrorDescription(description) {
  return {
    type: "GET_REGISTRATION_ERROR_DESCRIPTION",
    registration_error_description: description
  }
}

export const clearRegistrationErrorDescription = () => {
  return (dispatch) => {
    dispatch(clearRegistrationErrorDescriptionFn())
  }
}

export function clearRegistrationErrorDescriptionFn() {
  return {
    type: "GET_REGISTRATION_ERROR_DESCRIPTION",
    registration_error_description: false
  }
}

// --------------------------------------------------------------------------------------------
// отправка e-mail для восстановления пароля
export const sendForgetPassword = (webSocket, item) => {
  return (dispatch) => {
    webSocket.emit('forget_password', item)
    webSocket.on('forget_password', (res) => {
      let parse = JSON.parse(res)
      dispatch(getForgetPasswordAnswer(parse.status))
    })
  }
}

export function getForgetPasswordAnswer(description) {
  return {
    type: "GET_FORGET_PASSWORD_ANSWER",
    answer: description
  }
}

// --------------------------------------------------------------------------------------------
// получение тасков в сайдбар
export const getTaskTotal = (webSocket) => {
  return (dispatch) => {
    let data = {
      'access_token': localStorage.getItem('pr_store_token'),
      'method': 'taskTotal',
      'data': {}
    }
    webSocket.send(JSON.stringify(data))
  
    webSocket.onmessage = (evt) => {
      let parse = JSON.parse(evt.data)
      switch (parse['method']) {
        case 'taskTotal':
          dispatch(taskTotalFn(parse.data))
          break
        default:
          return null
      }
    }
  }
}

export function taskTotalFn(taskTotal) {
  return {
    type: "GET_TASKTOTAL",
    taskTotal: taskTotal,
  }
}

// --------------------------------------------------------------------------------------------
// Установка текущего таска (instagram/vk/fb и т.д.)
export const setCurrentTask = (task) => {
  return (dispatch) => {
    dispatch(setCurrentTaskFn(task))
  }
}

export function setCurrentTaskFn(task) {
  return {
    type: "SET_CURRENT_TASK",
    currentTask: task,
  }
}

// --------------------------------------------------------------------------------------------
// Установка текущего типа таска (like/comment/share и т.д.)
export const setCurrentType = (type) => {
  return (dispatch) => {
    dispatch(setCurrentTypeFn(type))
  }
}

export function setCurrentTypeFn(type) {
  return {
    type: "SET_CURRENT_TYPE",
    currentType: type,
  }
}
// -----------------------------------------------------------------------------------------
// Получение тасков
export const taskBySocial = (webSocket, social, type, limit, offset) => {
  return (dispatch) => {
    dispatch(setLoaderGlobal(true))
    let data = {
      'access_token': localStorage.getItem('pr_store_token'),
      'method': 'task',
      'data': {
        social: social,
        type: type,
        limit: limit,
        offset: offset
      }
    }
    webSocket.onopen = () => {
      webSocket.send(JSON.stringify(data))
    }
    webSocket.onmessage = (evt) => {
      let parse = JSON.parse(evt.data)
      switch (parse['method']) {
        case 'task':
          dispatch(taskBySocialFn(parse.data.task_list))
          break
        case 'handler.error':
          console.log(parse.data.description)
          console.log(parse.data)
          break
        default:
          return null
      }
    }
  }
}

export function taskBySocialFn(taskSocial) {
  return {
    type: "GET_TASK_BY_SOCIAL",
    taskSocial: taskSocial,
  }
}

export function setLoaderGlobal(loader) {
  return {
    type: "GET_LOADER_GLOBAL",
    loaderGlobal: loader,
  }
}

export function clickCount() {
  return {
    type: "CLICK_COUNT",
    click_count: Math.random()
  }
}
