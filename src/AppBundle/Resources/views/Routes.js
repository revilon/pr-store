import React from 'react'
import {Route, Switch} from 'react-router'
import MainRoom from './containers/MainRoom'
import Login from './containers/Login'
import PageNotFound from './containers/PageNotFound'
import {connect} from 'react-redux'
import * as actionCreators from './actions'

const mapStateToProps = (state) => {
  return state
}

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" render={() => <MainRoom webSocket={this.props.webSocket}/>}/>
        <Route path="/login" render={() => <Login webSocket={this.props.webSocket}/>}/>
        <Route render={() => <PageNotFound/>}/>
      </Switch>
    )
  }
}

export default connect(mapStateToProps, actionCreators)(Routes)
