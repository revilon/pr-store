const initialState = {
  login_token: '',
  login_status: '',
  login_error_description: '',
  registration_token: '',
  registration_status: '',
  registration_error_description: '',
  taskTotal: '',
  taskSocial: '',
  currentTask: 'instagram',
  currentType: 'all',
  loaderGlobal: false,
  click_count: ''
}

export const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_LOGIN_TOKEN':
      return {
        ...state,
        login_token: action.login_token,
      }
    case 'GET_LOGIN_STATUS':
      return {
        ...state,
        login_status: action.login_status,
      }
    case 'GET_LOGIN_ERROR_DESCRIPTION':
      return {
        ...state,
        login_error_description: action.login_error_description,
      }
    case 'GET_REGISTRATION_TOKEN':
      return {
        ...state,
        registration_token: action.registration_token,
      }
    case 'GET_REGISTRATION_STATUS':
      return {
        ...state,
        registration_status: action.registration_status
      }
    case 'GET_REGISTRATION_ERROR_DESCRIPTION':
      return {
        ...state,
        registration_error_description: action.registration_error_description,
      }
    case 'GET_TASKTOTAL':
      return {
        ...state,
        taskTotal: action.taskTotal.task_total_list,
      }
    case 'GET_TASK_BY_SOCIAL':
      return {
        ...state,
        taskSocial: action.taskSocial,
      }
    case 'SET_CURRENT_TASK':
      return {
        ...state,
        currentTask: action.currentTask
      }
    case 'SET_CURRENT_TYPE':
      return {
        ...state,
        currentType: action.currentType
      }
    case 'GET_LOADER_GLOBAL':
      return {
        ...state,
        loaderGlobal: action.loaderGlobal
      }
      case 'CLICK_COUNT':
      return {
        ...state,
        click_count: action.click_count
      }
    default:
      return state
  }
}

export default mainReducer
