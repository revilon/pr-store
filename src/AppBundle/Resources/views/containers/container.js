import React from 'react'
import {connect} from 'react-redux'
import * as actionCreators from '../actions/index'
import Button from '../components/Button/index'

import io from "socket.io-client"
let socket


class Container extends React.Component {
  constructor(props) {
    super(props)
    socket = io.connect('https://pr-store.9ek.ru:49292', {secure: true, reconnect: true, rejectUnauthorized: false});
    socket.on('login', (res) => {
      console.log(res)
    })
    
    socket.on('handler.error',  (data) => {
      console.log(data);
    });
  }
  handleClick = () => {
    socket.emit('login', { email: 'test@test.ru', password: 'keklol' });
    console.log("нажато")
  }
  
  render() {
    const {data, count} = this.props
    
    return (
      <div>
        <button onClick={this.handleClick}>
          нажать
        </button>
        <Button height={30}> dasda</Button>
      </div>
    )
  }
}


const mapStateToProps = (state) => {
  return state
};

export default connect(mapStateToProps, actionCreators)(Container)
