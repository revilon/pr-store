import React from 'react'
import styled from 'styled-components'
import Logo from '../../components/SidebarMenu/Logo'
import SidebarMenu from '../../components/SidebarMenu'

const SideBarWrapper = styled.div`
  background-color: black;
  opacity: 0.9;
  position: relative;
  width: 260px;
  height: 100vh;
  color: #fff;
  @media (max-width: 767px) {
   display: none;
  }
`
const SidebarHeader = styled.div`
  display: flex;
  width: 100%;
  height: 60px;
  justify-content: center;
  align-items: center;
  color: #000;
  background-color: #fff;
  border-bottom: 1px solid hsla(0, 0%, 100%, 0.2);
`
const MenuWrapper = styled.div`
  width: 260px;
  height: 100%;
  overflow: hidden;
`
const HiddenScrollItemWrapper = styled.div`
  overflow-y: auto;
  height: 100%;
  margin-right: -50px;
  padding-right: 33px;
`

class Sidebar extends React.Component {
	render() {
		return (
			<SideBarWrapper>
				<SidebarHeader>
					<Logo />
				</SidebarHeader>
				<MenuWrapper>
					<HiddenScrollItemWrapper>
						<SidebarMenu webSocket={this.props.webSocket}/>
					</HiddenScrollItemWrapper>
				</MenuWrapper>
			</SideBarWrapper>
		)
	}
}

export default Sidebar
