import React from 'react'
import styled from 'styled-components'
import Template from '../../components/LoginTemplate'
import Login from '../../components/Login'
import ForgetPassword from '../../components/ForgetPassword'
import Loader from '../../components/Loader'
import Registration from '../../components/Registration'
import {ifProp} from 'styled-tools'

const Wrapper = styled.section`
  background-color: rgba(255,255,255,.85);
  padding: 20px;
  border-radius: 6px;
  width: 360px;
  height: 400px;
  color: #777;
  box-shadow: 0 4px 24px -1px rgba(0,0,0,.3);
`
const LoaderWrapper = styled.div`
  position: fixed;
  z-index: 9999;
  height: 100%;
  width: 100%;
  background-color: #ffffff4d;
`
const Section = styled.section`
  padding: 20px;
`
// eslint-disable-next-line
const LinkForgetPassword = styled.a`
  cursor: pointer;
  text-align: center;
  margin-top: auto;
`
const NavigationButtonsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  border-top: 1px solid #dadada;
`
const NavigationButton = styled.a`
  display: block;
  border-bottom: 7px solid ${ifProp('selected', '#8bb2da', 'transparent')};
  padding: 10px 5px 7px;
  cursor: pointer;
  width: 50%;
  margin: 0 10px;
  text-align: center;
  font-size: 14px;
  color: #222;
`
const Logo = styled.div`
  text-align: center;
  padding: 15px 5px;
  background-color: #fff;
  font-weight: bold;
  font-size: 25px;
`
const WrapperComponent = styled.div`
  display: flex;
  flex-direction: column;
  height: 280px;
`

class LoginPage extends React.Component {
  constructor(props) {
    super(props)
    this.onChangeComponentLogin = this.onChangeComponentLogin.bind(this)
    this.onChangeComponentRegistration = this.onChangeComponentRegistration.bind(this)
    this.setLoader = this.setLoader.bind(this)
  }
  
  state = {
    component: 'login',
    selected: true,
    forget_password: false,
    loader: false,
    error_loader: false
  }
  
  onChangeComponentLogin(type) {
    this.setState({
      ...this.state,
      component: type,
      selected: true
    })
  }
  
  onChangeComponentRegistration(type) {
    this.setState({
      ...this.state,
      component: type,
      selected: false
    })
  }
  
  // eslint-disable-next-line
  renderForgetPassword = () => {
    this.setState({
      ...this.state,
      forget_password: true
    })
  }
  
  onChangeForm() {
    this.setState({
      ...this.state,
      forget_password: false
    })
  }
  
  setLoader(loader, error) {
    this.setState({
      ...this.state,
      loader: loader,
      error_loader: error
    })
  }
  
  renderComponent = (type) => {
    switch (type) {
      case 'login':
        return (
          <WrapperComponent>
            <Login setLoader={this.setLoader} webSocket={this.props.webSocket}/>
            {/*<LinkForgetPassword onClick={this.renderForgetPassword}>*/}
            {/*Забыли пароль?*/}
            {/*</LinkForgetPassword>*/}
          </WrapperComponent>
        )
      case 'registration':
        return <Registration setLoader={this.setLoader} webSocket={this.props.webSocket}/>
      default:
        return null
    }
  }
  
  render() {
    const {component, selected, forget_password, loader, error_loader} = this.state
    
    return (
      <div>
        {loader && (
          <LoaderWrapper>
            <Loader error_loader={error_loader}/>
          </LoaderWrapper>
        )}
        <Template>
          <Wrapper>
            <Logo>
              PR-STORE
            </Logo>
            {!forget_password && (
              <div>
                <NavigationButtonsWrapper>
                  <NavigationButton
                    selected={selected}
                    onClick={() => {
                      this.onChangeComponentLogin('login')
                    }}>
                    Вход
                  </NavigationButton>
                  <NavigationButton
                    selected={!selected}
                    onClick={() => {
                      this.onChangeComponentRegistration('registration')
                    }}>
                    Регистрация
                  </NavigationButton>
                </NavigationButtonsWrapper>
                <Section>
                  {this.renderComponent(component)}
                </Section>
              </div>
            )}
            {forget_password && (
              <ForgetPassword onChangeForm={this.onChangeForm}/>
            )}
          </Wrapper>
        </Template>
      </div>
    )
  }
}

export default LoginPage
