import React from 'react'
import {connect} from 'react-redux'
import * as actionCreators from '../../actions'
import styled from 'styled-components'
import ReconnectingWebSocket from 'ReconnectingWebSocket'

import Template from '../../components/RoomTemplate'
import MainRoomHeader from '../../components/MainRoom/MainRoomHeader'
import Icon from '../../components/Fa-Icon'
import Button from '../../components/Button'

let webSocket = new ReconnectingWebSocket('wss://pr-store.9ek.ru:49161')

const mapStateToProps = (state) => {
  return state
}

const IconWrapper = styled.div`
  display: inline-block;
  padding: 0 10px;
  width: 40px;
  @media (max-width: 767px) {
   display: none;
  }
`
const ArticleWrapper = styled.article`
  margin-bottom: 20px;
`
const Item = styled.div`
  display: flex;
  align-items: center;
  border-radius: 3px;
  background-color: #fafbfc;
  border: 1px solid #ddd;
  padding: 15px 20px;
`
const Avatar = styled.img`
  width: 45px;
  height: 45px;
  border-radius: 50%;
`
const AvatarWrapper = styled.div`
  display: inline-block;
  padding: 0 10px;
`
const ButtonWrapper = styled.div`
  width: 90px;
  display: table-cell;
`
const TextWrapper = styled.div`
  font-size: 14px;
  width: 100%;
  @media (max-width: 767px) {
    font-size: 13px;
    -webkit-line-clamp: 2;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    word-wrap: break-word;
    width: 100%;
    display: inline-block;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  &:hover, &:active, &:focus {
  cursor: pointer;
    color: #2b96f1;
  }
`
const Text = styled.a`
  display: inline-block;
`
const TextLink = styled.p`
  display: inline-block;
  margin-left: 5px;
  color: #2b96f1;
`
const MainRoomFixedCentre = styled.div`
  max-width: 880px;
  display: block;
  margin-left: auto;
  margin-right: auto;
  padding: 20px 20px 0;
`

let renderItems

class MainRoom extends React.Component {
  constructor(props) {
    super(props)
    this.sendData = this.sendData.bind(this)
    
    if (!localStorage.getItem('pr_store_token')) {
      window.location = '/login'
    }
  }
  
  componentWillMount() {
    webSocket.onopen = () => {
      console.log('webSocket', webSocket);
    }
    this.props.taskBySocial(webSocket, 'instagram')
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.click_count !== this.props.click_count) {
      this.sendData(nextProps.currentTask, nextProps.currentType)
    }
  }
  
  sendData(currentTask, currentType) {
    let type
    if (currentType === 'all') {
      type = ''
    } else {
      type = currentType
    }
    webSocket.send(JSON.stringify({
      'access_token': localStorage.getItem('pr_store_token'),
      'method': 'task',
      'data': {
        social: currentTask,
        type: type
      }
    }))
  }
  
  state = {
    tasks: '',
    buttonPalette: 'primary'
  }
  
  onOpenNewWindow(link) {
    let tab;
    let tabHeight = 750;
    let tabWidth = 750;
    let tabSettings = ''
      + 'menubar=no,'
      + 'toolbar=no,'
      + 'location=no,'
      + 'status=no,'
      + 'resizable=yes,'
      + 'scrollbars=yes,'
      + 'height=' + tabHeight + ','
      + 'width=' + tabWidth + ','
      + 'left=' + (window.screen.width / 2 - tabHeight / 2) + ','
      + 'top=' + (window.screen.height / 2 - 100 / 2);
    
    tab = window.open(link, '_blank', tabSettings);
    
    setInterval(function () {
      if (tab.closed === true) {
        console.log('Tab closed');
      } else {
        console.log('Tab open');
      }
    }, 1000);
  }
  
  renderCostButton = (cost, link) => {
    let point
    
    switch (cost) {
      case cost === 1:
        point = cost + ' балл'
        break
      case cost > 1 && cost <= 4:
        point = cost + ' балла'
        break
      default:
        point = cost + ' баллов'
    }
    
    return (
      <ButtonWrapper>
        <Button
          palette={this.state.buttonPalette}
          height={28}
          onClick={() => this.onOpenNewWindow(link)}
        >
          {point}
        </Button>
      </ButtonWrapper>
    )
  }
  
  render() {
    const {currentTask, currentType, taskSocial} = this.props
    
    if (taskSocial) {
      renderItems = taskSocial.map((prop, key) => (
        <ArticleWrapper key={key}>
          <Item>
            <IconWrapper>
              <Icon height={28} name={`${currentTask} fab`}/>
            </IconWrapper>
            <AvatarWrapper>
              <Avatar src={prop.image_url} alt=""/>
            </AvatarWrapper>
            <TextWrapper>
              <Text onClick={() => this.onOpenNewWindow(prop.link)}>
                {prop.text}
                <TextLink>[name]</TextLink>
              </Text>
            </TextWrapper>
            {this.renderCostButton(prop.cost, prop.link)}
          </Item>
        </ArticleWrapper>
      ))
    }
    
    return (
      taskSocial && (
        <Template webSocket={webSocket}>
          <MainRoomFixedCentre>
            <MainRoomHeader currentTask={currentTask} currentType={currentType}/>
            <div>{renderItems}</div>
          </MainRoomFixedCentre>
        </Template>
      )
    )
  }
}

export default connect(mapStateToProps, actionCreators)(MainRoom)
