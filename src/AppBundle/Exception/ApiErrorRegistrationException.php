<?php

namespace AppBundle\Exception;

/**
 * Class ApiErrorRegistrationException
 * @package AppBundle\Exception
 */
class ApiErrorRegistrationException extends ApiErrorException
{
}
