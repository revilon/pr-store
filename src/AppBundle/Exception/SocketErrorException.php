<?php

namespace AppBundle\Exception;

use Exception;

/**
 * Class SocketErrorException
 * @package AppBundle\Exception
 */
class SocketErrorException extends Exception
{
    /**
     * SocketErrorException constructor.
     * @param int $code
     * @param string $message
     * @param array $trace
     */
    public function __construct(int $code = 0, string $message = '', array $trace = [])
    {
        parent::__construct($message, $code);
    }
}
