<?php

namespace AppBundle\Exception;

/**
 * Class ApiErrorTaskTotalException
 * @package AppBundle\Exception
 */
class ApiErrorTaskTotalException extends ApiErrorException
{
}
