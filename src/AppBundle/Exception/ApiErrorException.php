<?php

namespace AppBundle\Exception;

use Exception;

/**
 * Class ApiErrorException
 * @package AppBundle\Exception
 */
class ApiErrorException extends Exception
{
    public const METHOD = 'handler.error';

    /** @var string */
    private $parentMethod;

    /**
     * ApiErrorException constructor.
     * @param int $code
     * @param string $message
     * @param array $trace
     */
    public function __construct(int $code = 0, string $message = '', array $trace = [])
    {
        if (empty($message)) {
            $message = (string)$code;
        }
        parent::__construct($message, $code);
    }

    /**
     * @param string $parentMethod
     * @return ApiErrorException
     */
    public function setParentMethod(string $parentMethod): self
    {
        $this->parentMethod = $parentMethod;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentMethod()
    {
        return $this->parentMethod;
    }
}
