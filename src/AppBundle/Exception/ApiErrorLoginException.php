<?php

namespace AppBundle\Exception;

/**
 * Class ApiErrorLoginException
 * @package AppBundle\Exception
 */
class ApiErrorLoginException extends ApiErrorException
{
}
