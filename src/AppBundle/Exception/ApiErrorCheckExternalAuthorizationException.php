<?php

namespace AppBundle\Exception;

/**
 * Class ApiErrorCheckExternalAuthorizationException
 * @package AppBundle\Exception
 */
class ApiErrorCheckExternalAuthorizationException extends ApiErrorException
{
}
