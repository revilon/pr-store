<?php

namespace AppBundle\Exception;

/**
 * Class ApiErrorException
 * @package AppBundle\Exception
 */
class ApiErrorCodeEnum
{
    public const LOGIN_AUTH_FAIL = 1000;

    public const USER_NOT_REGISTER = 1001;
    public const USER_EXIST = 1002;
    public const ERROR_VALIDATE = 1003;

    public const REQUIRED_AUTH_TOKEN = 2000;
    public const WRONG_AUTH_TOKEN = 2001;

    public const QUERY_PARSE_ERROR = 3000;

    public const EXTERNAL_AUTH = 4001;

    public const SOCKET_CONNECTION_NOT_EXIST = 4000;

    public const UNEXPECTED_ERROR = 100;
}
