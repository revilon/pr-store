<?php

namespace AppBundle\Service\Registration;

use AppBundle\Entity\User;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorRegistrationException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class RegistrationHandler
 * @package AppBundle\Service\Registration
 */
class RegistrationHandler
{
    /**
     * RegistrationHandler constructor.
     * @param ContainerInterface $container
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->container = $container;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @return string
     */
    public function makeToken(): string
    {
        $uniqueString = uniqid('_tk', true) . random_int(0, 10 * 10);

        return password_hash($uniqueString, PASSWORD_DEFAULT);
    }

    public function handle(RegistrationQuery $query): array
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $userRepository = $em->getRepository(User::class);

        $user = $userRepository->findOneBy([
            'email' => $query->getEmail(),
        ]);

        if ($user instanceof User) {
            throw new ApiErrorRegistrationException(ApiErrorCodeEnum::USER_EXIST);
        }

        $user = new User();
        $user->setEmail($query->getEmail());

        $user->setPlainPassword($query->getPassword());
        $password = $this->passwordEncoder->encodePassword($user, $query->getPassword());
        $user->setPassword($password);

        $user->setUserAgent($query->getUserAgent());
        $user->setIp($query->getIp());
        $user->setLastLoginAt(new \DateTime());

        $token = $this->makeToken();
        $user->setToken($token);

        $em->persist($user);
        $em->flush();

        return [
            'token' => $token,
        ];
    }
}
