<?php

namespace AppBundle\Service\Registration;

use AppBundle\Dto\AbstractDto;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationQuery
 * @package AppBundle\Service\Registration
 *
 * @method $this setEmail(string $email)
 * @method string getEmail()
 * @method $this setPassword(string $password)
 * @method string getPassword()
 */
class RegistrationQuery extends AbstractDto
{
    public const METHOD = 'registration';

    /** @var string */
    private $userAgent;

    /** @var string */
    private $ip;

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setRequired([
            'email',
            'password',
        ]);

        $options->setAllowedTypes('email', 'string');
        $options->setAllowedTypes('password', 'string');
    }

    /**
     * @param string $userAgent
     * @return RegistrationQuery
     */
    public function setUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @param string $ip
     * @return RegistrationQuery
     */
    public function setIp(string $ip): RegistrationQuery
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }
}
