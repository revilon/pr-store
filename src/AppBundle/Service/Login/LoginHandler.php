<?php

namespace AppBundle\Service\Login;

use AppBundle\Entity\User;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorLoginException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class RegistrationHandler
 * @package AppBundle\Service\Login
 */
class LoginHandler
{
    /**
     * LoginHandler constructor.
     * @param ContainerInterface $container
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->container = $container;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @return string
     */
    public function makeToken(): string
    {
        $uniqueString = uniqid('_tk', true) . random_int(0, 10 * 10);

        return password_hash($uniqueString, PASSWORD_DEFAULT);
    }

    public function handle(LoginQuery $query): array
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $userRepository = $em->getRepository(User::class);

        /** @var User $user */
        $user = $userRepository->findOneBy([
            'email' => $query->getEmail(),
        ]);

        if (!$user instanceof User) {
            throw new ApiErrorLoginException(ApiErrorCodeEnum::USER_NOT_REGISTER);
        }

        $passwordValid = $this->passwordEncoder->isPasswordValid($user, $query->getPassword());

        if ($passwordValid === false) {
            throw new ApiErrorLoginException(ApiErrorCodeEnum::USER_NOT_REGISTER);
        }

        $user->setLastLoginAt(new \DateTime());

        $em->persist($user);
        $em->flush();

        return [
            'token' => $user->getToken(),
        ];
    }
}
