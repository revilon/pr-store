<?php

namespace AppBundle\Service\Login;

use AppBundle\Dto\AbstractDto;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoginQuery
 * @package AppBundle\Service\Login
 *
 * @method $this setEmail(string $email)
 * @method string getEmail()
 * @method $this setPassword(string $password)
 * @method string getPassword()
 */
class LoginQuery extends AbstractDto
{
    public const METHOD = 'login';

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setRequired([
            'email',
            'password',
        ]);

        $options->setAllowedTypes('email', 'string');
        $options->setAllowedTypes('password', 'string');
    }
}
