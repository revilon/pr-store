<?php

namespace AppBundle\Service\CheckCompleteTask;

use Doctrine\ORM\EntityManager;

/**
 * Class CheckCompleteTaskManager
 * @package AppBundle\Service\CheckCompleteTask
 */
class CheckCompleteTaskManager
{
    /**
     * CheckCompleteTaskManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
}
