<?php

namespace AppBundle\Service\CheckCompleteTask;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CheckCompleteTaskHandler
 * @package AppBundle\Service\Registration
 */
class CheckCompleteTaskHandler
{
    /**
     * CheckCompleteTaskHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param CheckCompleteTaskQuery $query
     * @return array
     */
    public function handle(CheckCompleteTaskQuery $query): array
    {
    }
}
