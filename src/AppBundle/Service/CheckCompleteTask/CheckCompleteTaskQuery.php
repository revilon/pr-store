<?php

namespace AppBundle\Service\CheckCompleteTask;

use AppBundle\Dto\AbstractDto;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CheckCompleteTaskQuery
 * @package AppBundle\Service\CheckCompleteTask
 *
 * @method int getTaskId()
 * @method int getUserId()
 */
class CheckCompleteTaskQuery extends AbstractDto
{
    public const METHOD = 'checkCompleteTask';

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setRequired([
            'task_id',
            'user_id',
        ]);

        $options->setAllowedTypes('task_id', 'int');
        $options->setAllowedTypes('user_id', 'int');
    }
}
