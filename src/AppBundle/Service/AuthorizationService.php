<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AuthorizationService
 * @package AppBundle\Service\Login
 */
class AuthorizationService
{
    /**
     * AuthorizationService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param null|string $token
     * @return User
     * @throws ApiErrorException
     */
    public function getUser(?string $token): User
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $userRepository = $em->getRepository(User::class);

        $user = $userRepository->findOneBy(['token' => $token]);

        if (!$user instanceof User) {
            throw new ApiErrorException(ApiErrorCodeEnum::WRONG_AUTH_TOKEN);
        }

        return $user;
    }

    /**
     * @param null|string $token
     * @return void
     * @throws ApiErrorException
     */
    public function check(?string $token): void
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $userRepository = $em->getRepository(User::class);

        $user = $userRepository->findOneBy(['token' => $token]);

        if (!$user instanceof User) {
            throw new ApiErrorException(ApiErrorCodeEnum::WRONG_AUTH_TOKEN);
        }
    }
}
