<?php

namespace AppBundle\Service\TaskTotal;

/**
 * Class TaskQuery
 * @package AppBundle\Service\TaskTotal
 */
class TaskTotalQuery
{
    public const METHOD = 'taskTotal';
}
