<?php

namespace AppBundle\Service\TaskTotal;

use AppBundle\Enum\SocialEnum;

/**
 * Class TaskTotalFormatter
 * @package AppBundle\Service\TaskTotal
 */
class TaskTotalFormatter
{
    public function format(array $data): array
    {
        $totalList = [];
        foreach ($data as $item) {
            $lowerType = strtolower($item['type']);
            $lowerSocial = strtolower($item['social']);

            $totalList[$lowerSocial][$lowerType] = (int)$item['count'];
        }

        $resultTotalList = [];
        foreach ($totalList as $social => $total) {
            $socialName = SocialEnum::getChoices()[strtoupper($social)];

            $resultTotalList[] = [
                'id' => $social,
                'name' => $socialName,
                'task_type' => $total,
            ];
        }

        return [
            'task_total_list' => $resultTotalList,
        ];
    }
}
