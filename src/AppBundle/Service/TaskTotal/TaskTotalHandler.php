<?php

namespace AppBundle\Service\TaskTotal;

use AppBundle\Entity\Task;
use AppBundle\Enum\TaskStatusEnum;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaskTotalHandler
 * @package AppBundle\Service\TaskTotal
 */
class TaskTotalHandler
{
    /**
     * TaskTotalHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function handle(): array
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $taskRepository = $em->getRepository(Task::class);
        $queryBuilder = $taskRepository->createQueryBuilder('t');
        $queryBuilder
            ->select('t.social')
            ->addSelect('t.type')
            ->addSelect(
                $queryBuilder->expr()->count('t.type') . 'as count'
            )
            ->where('t.status = :status')
            ->groupBy('t.social, t.type')
            ->setParameter('status', TaskStatusEnum::ACTIVE)
        ;

        return $queryBuilder->getQuery()->execute();
    }
}
