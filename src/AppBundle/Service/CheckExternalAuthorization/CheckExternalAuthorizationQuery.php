<?php

namespace AppBundle\Service\CheckExternalAuthorization;

use AppBundle\Entity\User;
use AppBundle\Dto\AbstractDto;
use AppBundle\Enum\SocialEnum;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CheckExternalAuthorizationQuery
 * @package AppBundle\Service\CheckExternalAuthorization
 *
 * @method string getSocial()
 * @method User getUser()
 */
class CheckExternalAuthorizationQuery extends AbstractDto
{
    public const METHOD = 'checkExternalAuthorization';

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setRequired([
            'social',
            'user',
        ]);

        $socialAllowedValues = array_map('strtolower', SocialEnum::getValues());

        $options->setAllowedTypes('user', User::class);
        $options->setAllowedTypes('social', 'string');

        $options->setAllowedValues('social', $socialAllowedValues);

        $options->setNormalizer('social', function (Options $options, $value) {
            return strtoupper($value);
        });
    }
}
