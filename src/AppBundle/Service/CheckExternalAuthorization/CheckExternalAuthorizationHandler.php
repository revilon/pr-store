<?php

namespace AppBundle\Service\CheckExternalAuthorization;

use AppBundle\Entity\ExternalAuth;
use AppBundle\Exception\ApiErrorCodeEnum;
use AppBundle\Exception\ApiErrorCheckExternalAuthorizationException;

/**
 * Class CheckExternalAuthorizationHandler
 * @package AppBundle\Service\CheckExternalAuthorization
 */
class CheckExternalAuthorizationHandler
{
    /**
     * CheckExternalAuthorizationHandler constructor.
     * @param CheckExternalAuthorizationManager $manager
     */
    public function __construct(CheckExternalAuthorizationManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param CheckExternalAuthorizationQuery $query
     * @return array
     * @throws ApiErrorCheckExternalAuthorizationException
     */
    public function handle(CheckExternalAuthorizationQuery $query): array
    {
        $externalAuth = $this->manager->getExternalAuthorization($query);

        if ($externalAuth instanceof ExternalAuth) {
            $result = [
                'external_id' => $externalAuth->getExternalId(),
                'social_type' => $externalAuth->getSocial(),
            ];
        } else {
            throw new ApiErrorCheckExternalAuthorizationException(ApiErrorCodeEnum::EXTERNAL_AUTH);
        }

        return $result;
    }
}
