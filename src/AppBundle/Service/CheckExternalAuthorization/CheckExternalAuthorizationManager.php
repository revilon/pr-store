<?php

namespace AppBundle\Service\CheckExternalAuthorization;

use AppBundle\Entity\ExternalAuth;
use AppBundle\Enum\ExternalAuthStatusEnum;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CheckExternalAuthorizationManager
 * @package AppBundle\Service\CheckExternalAuthorization
 */
class CheckExternalAuthorizationManager
{
    /**
     * CheckExternalAuthorizationHandler constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CheckExternalAuthorizationQuery $query
     * @return ExternalAuth|null
     */
    public function getExternalAuthorization(CheckExternalAuthorizationQuery $query): ?ExternalAuth
    {
        $externalAuthRepository = $this->em->getRepository(ExternalAuth::class);

        return $externalAuthRepository->findOneBy([
            'social' => $query->getSocial(),
            'user' => $query->getUser(),
            'status' => ExternalAuthStatusEnum::ACTIVE,
        ]);
    }
}
