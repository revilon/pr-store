<?php

namespace AppBundle\Service\CheckExternalAuthorization;

/**
 * Class CheckExternalAuthorizationFormatter
 * @package AppBundle\Service\CheckExternalAuthorization
 */
class CheckExternalAuthorizationFormatter
{
    /**
     * @param array $data
     * @return array
     */
    public function format(array $data): array
    {
        $data['social_type'] = strtolower($data['social_type']);

        return $data;
    }
}
