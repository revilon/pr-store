<?php

namespace AppBundle\Service;

use AppBundle\Dto\VkUserDto;
use VK\Client\VKApiClient;

/**
 * Class VkAuthService
 * @package AppBundle\Service
 */
class VkAuthService
{
    /**
     * @param VkUserDto $vkUser
     * @return void
     */
    public function fetchUser(VkUserDto $vkUser): void
    {
        $vk = new VKApiClient();
        $response = $vk->users()->get($vkUser->getAccessToken(), [
            'fields' => [
                'photo_id',
                'verified',
                'sex',
                'bdate',
                'city',
                'country',
                'home_town',
                'has_photo',
                'photo_50',
                'photo_100',
                'photo_200_orig',
                'photo_200',
                'photo_400_orig',
                'photo_max',
                'photo_max_orig',
                'online',
                'domain',
                'has_mobile',
                'contacts',
                'site',
                'education',
                'universities',
                'schools',
                'status',
                'last_seen',
                'followers_count',
                'common_count',
                'occupation',
                'nickname',
                'relatives',
                'relation',
                'personal',
                'connections',
                'exports',
                'wall_comments',
                'activities',
                'interests',
                'music',
                'movies',
                'tv',
                'books',
                'games',
                'about',
                'quotes',
                'can_post',
                'can_see_all_posts',
                'can_see_audio',
                'can_write_private_message',
                'can_send_friend_request',
                'is_favorite',
                'is_hidden_from_feed',
                'timezone',
                'screen_name',
                'maiden_name',
                'crop_photo',
                'is_friend',
                'friend_status',
                'career',
                'military',
                'blacklisted',
                'blacklisted_by_m'
            ]
        ]);

        $user = array_shift($response);
        $vkUser->resolve([
            'id' => $user['id'] ?? null,
            'full_name' => $user['first_name'] ?? null . ' ' . $user['last_name'] ?? null,
            'sex' => $user['sex'] ?? null,
            'nickname' => $user['nickname'] ?? null,
            'domain' => $user['domain'] ?? null,
            'bdate' => $user['bdate'] ?? null,
            'city' => $user['city']['title'] ?? null,
            'country' => $user['country']['title'] ?? null,
            'timezone' => $user['timezone'] ?? null,
            'photo_max_orig' => $user['photo_max_orig'] ?? null,
            'photo_id' => $user['photo_id'] ?? null,
            'has_photo' => $user['has_photo'] ?? null,
            'has_mobile' => $user['has_mobile'] ?? null,
            'online' => $user['online'] ?? null,
            'wall_comments' => $user['wall_comments'] ?? null,
            'can_post' => $user['can_post'] ?? null,
            'can_see_all_posts' => $user['can_see_all_posts'] ?? null,
            'can_see_audio' => $user['can_see_audio'] ?? null,
            'can_write_private_message' => $user['can_write_private_message'] ?? null,
            'can_send_friend_request' => $user['can_send_friend_request'] ?? null,
            'mobile_phone' => $user['mobile_phone'] ?? null,
            'home_phone' => $user['home_phone'] ?? null,
            'facebook' => $user['facebook'] ?? null,
            'facebook_name' => $user['facebook_name'] ?? null,
            'instagram' => $user['instagram'] ?? null,
            'site' => $user['site'] ?? null,
            'status_audio' => $user['status_audio']['artist'] ?? null . ' ' . $user['status_audio']['title'] ?? null,
            'status' => $user['status'] ?? null,
            'last_seen_unix' => $user['last_seen']['time'] ?? null,
            'followers_count' => $user['followers_count'] ?? null,
        ]);
    }
}
