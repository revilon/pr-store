<?php

namespace AppBundle\Service\Task;

use AppBundle\Entity\Task;

/**
 * Class TaskFormatter
 * @package AppBundle\Service\Task
 */
class TaskFormatter
{
    /**
     * @param Task[] $data
     * @param bool $isHasMore
     * @return array
     */
    public function format(array $data, bool $isHasMore): array
    {
        $taskList = [];
        foreach ($data as $task) {
            $taskList[] = [
                'id' => $task->getId(),
                'type_id' => $task->getType(),
                'social_id' => $task->getSocial(),
                'text' => $task->getText(),
                'link' => $task->getLink(),
                'image_url' => $task->getImageUrl(),
                'cost' => $task->getCost(),
                'name' => $task->getName(),
            ];
        }

        return [
            'has_more' => $isHasMore,
            'task_list' => $taskList,
        ];
    }
}
