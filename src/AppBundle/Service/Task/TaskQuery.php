<?php

namespace AppBundle\Service\Task;

use AppBundle\Dto\AbstractDto;
use AppBundle\Enum\SocialEnum;
use AppBundle\Enum\TaskTypeEnum;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TaskQuery
 * @package AppBundle\Service\Task
 *
 * @method string getSocial()
 * @method string getType()
 * @method int getLimit()
 * @method int getOffset()
 */
class TaskQuery extends AbstractDto
{
    public const METHOD = 'task';

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setRequired([
            'social',
        ]);

        $options->setDefined([
            'type',
            'limit',
            'offset',
        ]);

        $options->setDefaults([
            'type' => '',
            'limit' => 10,
            'offset' => 0,
        ]);

        $options->setAllowedTypes('social', 'string');
        $options->setAllowedTypes('type', 'string');
        $options->setAllowedTypes('limit', 'int');
        $options->setAllowedTypes('offset', 'int');

        $socialAllowedValues = array_map('strtolower', SocialEnum::getValues());
        $typeAllowedValues = array_map('strtolower', TaskTypeEnum::getValues());
        $typeAllowedValues[] = '';

        $options->setAllowedValues('social', $socialAllowedValues);
        $options->setAllowedValues('type', $typeAllowedValues);

        $options->setNormalizer('social', function (Options $options, $value) {
            return strtoupper($value);
        });

        $options->setNormalizer('type', function (Options $options, $value) {
            return strtoupper($value);
        });
    }
}
