<?php

namespace AppBundle\Service\Task;

use AppBundle\Entity\Task;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaskHandler
 * @package AppBundle\Service\Task
 */
class TaskHandler
{
    /**
     * TaskHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->manager = $container->get(TaskManager::class);
    }

    /**
     * @param TaskQuery $query
     * @return Task[]|array
     */
    public function handle(TaskQuery $query): array
    {
        if ($query->getSocial() && $query->getType()) {
            $taskList = $this->manager->taskBySocialType($query);
        } else {
            $taskList = $this->manager->taskBySocial($query);
        }

        $isHasMore = false;
        if (count($taskList) > $query->getLimit()) {
            $isHasMore = true;
            array_pop($taskList);
        }

        return [$taskList, $isHasMore];
    }
}
