<?php

namespace AppBundle\Service\Task;

use AppBundle\Entity\Task;
use AppBundle\Enum\TaskStatusEnum;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaskManager
 * @package AppBundle\Service\Task
 */
class TaskManager
{
    /**
     * TaskHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param TaskQuery $query
     * @return Task[]|array
     */
    public function taskBySocialType(TaskQuery $query): array
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $taskRepository = $em->getRepository(Task::class);

        return $taskRepository->findBy(
            [
                'status' => TaskStatusEnum::ACTIVE,
                'type' => $query->getType(),
                'social' => $query->getSocial()
            ],
            ['cost' => 'DESC'],
            $query->getLimit() + 1,
            $query->getOffset()
        );
    }

    /**
     * @param TaskQuery $query
     * @return Task[]|array
     */
    public function taskBySocial(TaskQuery $query): array
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $taskRepository = $em->getRepository(Task::class);

        return $taskRepository->findBy(
            [
                'status' => TaskStatusEnum::ACTIVE,
                'social' => $query->getSocial()
            ],
            ['cost' => 'DESC'],
            $query->getLimit() + 1,
            $query->getOffset()
        );
    }
}
