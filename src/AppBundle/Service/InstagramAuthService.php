<?php

namespace AppBundle\Service;

use AppBundle\Dto\InstagramUserDto;
use GuzzleHttp\Client;

/**
 * Class InstagramAuthService
 * @package AppBundle\Service
 */
class InstagramAuthService
{
    /**
     * @param InstagramUserDto $instagramUser
     * @return void
     */
    public function fetchUser(InstagramUserDto $instagramUser): void
    {
        $client = new Client();

        $token = $instagramUser->getAccessToken();

        $profileUrlPattern = 'https://api.instagram.com/v1/users/self/?access_token=%s';

        $response = $client->request('GET', sprintf($profileUrlPattern, $token));

        $response = $response->getBody()->getContents();
        $response = json_decode($response, true);

        $instagramUser->resolve([
            'id' => $response['data']['id'] ?? null,
            'username' => $response['data']['username'] ?? null,
            'profile_picture_url' => $response['data']['profile_picture'] ?? null,
            'full_name' => $response['data']['full_name'] ?? null,
            'bio' => $response['data']['bio'] ?? null,
            'website' => $response['data']['website'] ?? null,
            'is_business' => $response['data']['is_business'] ?? null,
            'media' => $response['data']['counts']['media'] ?? null,
            'follows' => $response['data']['counts']['follows'] ?? null,
            'followed_by' => $response['data']['counts']['followed_by'] ?? null,
        ]);
    }
}
