<?php

namespace AppBundle\Set;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use InvalidArgumentException;

/**
 * Class SetTypeBase
 * @package AppBundle\Set
 */
abstract class SetTypeBase extends Type
{
    public const NAME = null;
    public const BASE_SET_CLASS = null;

    /**
     * {@inheritdoc}
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        $values = implode(
            ', ',
            array_map(
                function ($value) {
                    return sprintf("'%s'", $value);
                },
                static::getValues()
            )
        );

        return sprintf('SET(%s)', $values);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return [];
        }

        $valueList = explode(',', $value);

        return array_combine($valueList, $valueList);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!\is_array($value) || \count($value) <= 0) {
            return null;
        }

        $diff = array_diff($value, static::getValues());

        if (\count($diff) > 0) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value "%s". It is not defined in "%s"',
                    implode(',', $diff),
                    \get_class($this)
                )
            );
        }

        return implode(',', $value);
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): ?string
    {
        return static::NAME;
    }

    /**
     * @return array
     */
    public static function getChoices(): array
    {
        /** @var SetBase $baseEnumClass */
        $baseEnumClass = static::BASE_SET_CLASS;

        return $baseEnumClass::getListCombine();
    }

    /**
     * @return array
     */
    public static function getValues(): array
    {
        return array_keys(static::getChoices());
    }
}
