<?php

namespace AppBundle\Set;

use ReflectionClass;

/**
 * Class SetBase
 * @package AppBundle\Set
 */
abstract class SetBase
{
    /**
     * @return array
     */
    public static function getValues(): array
    {
        $class = new ReflectionClass(static::class);

        return array_values($class->getConstants());
    }

    /**
     * @return array
     */
    public static function getList(): array
    {
        $class = new ReflectionClass(static::class);

        return $class->getConstants();
    }

    /**
     * @return array
     */
    public static function getListCombine(): array
    {
        $class = new ReflectionClass(static::class);

        return array_combine($class->getConstants(), $class->getConstants());
    }

    /**
     * @param string $value
     * @return int
     */
    public static function getBit(string $value): int
    {
        $constants = static::getList();

        $index = 0;

        foreach ($constants as $constant) {
            if (mb_strtolower($constant) === mb_strtolower($value)) {
                return 1 << $index;
            }
            $index++;
        }

        return 0;
    }
}
