<?php

namespace AppBundle\Type;

use AppBundle\Enum\EnumTypeBase;
use AppBundle\Enum\TaskTypeEnum;

/**
 * Class TaskTypeEnumType
 * @package AppBundle\Type
 */
final class TaskTypeEnumType extends EnumTypeBase
{
    public const NAME = 'task_type_enum';
    public const BASE_ENUM_CLASS = TaskTypeEnum::class;
}
