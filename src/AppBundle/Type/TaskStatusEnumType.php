<?php

namespace AppBundle\Type;

use AppBundle\Enum\EnumTypeBase;
use AppBundle\Enum\TaskStatusEnum;

/**
 * Class TaskStatusEnumType
 * @package AppBundle\Type
 */
final class TaskStatusEnumType extends EnumTypeBase
{
    public const NAME = 'task_status_enum';
    public const BASE_ENUM_CLASS = TaskStatusEnum::class;
}
