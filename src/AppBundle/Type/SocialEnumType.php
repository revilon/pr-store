<?php

namespace AppBundle\Type;

use AppBundle\Enum\EnumTypeBase;
use AppBundle\Enum\SocialEnum;

/**
 * Class SocialEnumType
 * @package AppBundle\Type
 */
final class SocialEnumType extends EnumTypeBase
{
    public const NAME = 'social_enum';
    public const BASE_ENUM_CLASS = SocialEnum::class;
}
