<?php

namespace AppBundle\Type;

use AppBundle\Enum\EnumTypeBase;
use AppBundle\Enum\ExternalAuthStatusEnum;

/**
 * Class ExternalAuthStatusEnumType
 * @package AppBundle\Type
 */
final class ExternalAuthStatusEnumType extends EnumTypeBase
{
    public const NAME = 'external_auth_status_enum';
    public const BASE_ENUM_CLASS = ExternalAuthStatusEnum::class;
}
